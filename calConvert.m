function [] = calConvert( calFile )
% 
% if nargin<1
%     [calName,calDir] = uigetfile(pwd,'Select the LFIT calibration file.');
%     calPath = fullfile(calDir,calName);
% end

% Load LFIT calibration
    load( calFile, 'cal' );
% Output file name
%     [calDir,calName,~] = fileparts(calFile);
    [calDir,~,~] = fileparts(calFile);
%     calName = strrep( calName, '_calibration','' );
    calName = 'camera0.drg-mcal';
% Save combined file
    fid = fopen( fullfile(calDir,calName), 'w' );
    fprintf( fid, '%d\n', cal.numS );
    fprintf( fid, '%d\n', cal.numT );
    for k=1:numel(cal.exactX)
        fprintf( fid, '%f,%f\n', cal.exactX(k)-1,cal.exactY(k)-1 );
    end
    fclose(fid);


