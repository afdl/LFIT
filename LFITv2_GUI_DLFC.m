function varargout = LFITv2_GUI_DLFC(varargin)
% LFITV2_GUI_DLFC MATLAB code for LFITv2_GUI_DLFC.fig
%      LFITV2_GUI_DLFC, by itself, creates a new LFITV2_GUI_DLFC or raises the existing
%      singleton*.
%
%      H = LFITV2_GUI_DLFC returns the handle to a new LFITV2_GUI_DLFC or the handle to
%      the existing singleton*.
%
%      LFITV2_GUI_DLFC('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in LFITV2_GUI_DLFC.M with the given input arguments.
%
%      LFITV2_GUI_DLFC('Property','Value',...) creates a new LFITV2_GUI_DLFC or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before LFITv2_GUI_DLFC_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to LFITv2_GUI_DLFC_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help LFITv2_GUI_DLFC

% Last Modified by GUIDE v2.5 06-Jan-2022 14:49:19

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @LFITv2_GUI_DLFC_OpeningFcn, ...
                   'gui_OutputFcn',  @LFITv2_GUI_DLFC_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before LFITv2_GUI_DLFC is made visible.
function LFITv2_GUI_DLFC_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to LFITv2_GUI_DLFC (see VARARGIN)

handles = setDefaults(hObject,handles);

% Choose default command line output for LFITv2_GUI_DLFC
handles.output = hObject;

% Main header image
axes(handles.tagHeaderIm);
imshow('header.png');

% Image showing uvSampling
axes(handles.tagUVimg);
imshow('uvlines.png');

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes LFITv2_GUI_DLFC wait for user response (see UIRESUME)
uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = LFITv2_GUI_DLFC_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
assignin('base','startProgram2',handles.startProgram2);
assignin('base','sensorSizeX',handles.sensorSizeX);
assignin('base','sensorSizeY',handles.sensorSizeY);
assignin('base','drg_path',handles.drg_path);
assignin('base','mainCal_path',handles.mainCal_path);
assignin('base','samplingMethod',handles.samplingMethod);
assignin('base','polyUni',handles.polyUni);
assignin('base','polyUV',handles.polyUV);
assignin('base','calOrder',handles.calOrder);
assignin('base','fNumber',handles.fNumber);

varargout{1} = handles.output;

closereq

function [handles] = setDefaults(hObject,handles)
% Read in workspace variables from prerun.m
workspaceVariables = evalin('base','who');
for k = 1:size(workspaceVariables,1)
    varName = workspaceVariables{k};
    temp = evalin('base',varName);
    eval([varName '=' 'temp;']); %into workspace
end

% Put default values into handles structure
handles.drg_path        = 'C:\TestFolder\Dragon.redist\';
handles.mainCal_path    = 'C:\TestFolder\Main\';
handles.mCal_path       = 'C:\TestFolder\Main\mCal\';
handles.numThreads      = 4;

% Directories
set(handles.tagTextDRG, 'String', handles.drg_path);
set(handles.tagTextMainCal, 'String', handles.mainCal_path);
set(handles.tagTextmCal, 'String', handles.mCal_path);
    
% General settings
handles.focalLength     = 60;
handles.Mag             = -0.5;
handles.fNumber         = 2;
handles.Planes          = [-10, -5, 0, 5, 10];
handles.nPixelX         = 900;
handles.nPixelY         = 600;
    
% Load whatever we can from prerun.m if it exists
if exist('magnification')
    set(handles.tagf_M, 'String', focLenMain);
    handles.focalLength = focLenMain;
    set(handles.tagMag, 'String', magnification);
    handles.Mag = magnification;
    set(handles.tagTextmCal, 'String', calFolderPath);
    handles.mCal_path       = calFolderPath;
end

% Step 1
handles.turtSampling    = 7;
handles.samplingMethod  = 'turtle';
% Step 2
handles.assignXYZPlane  = 0;
handles.squareSize      = 7;
handles.topLeftCoord    = [0, 0];
handles.diagnoseAll     = 0;
handles.run2            = 0;
% Step 3
handles.calOrder        = 3;
handles.polyUni         = 1;
handles.polyUV          = 0;
% Step 4
handles.xMin            = -30;
handles.xMax            = 30;
handles.yMin            = -20;
handles.yMax            = 20;

handles.startProgram2        = false;

% Camera Settings
handles.sensorSizeX     = 6600;
handles.sensorSizeY     = 4400;
handles.nMicroX         = 471;
handles.nMicroY         = 362;
handles.pixelPitch      = 0.0055;
handles.microPitch      = 0.077;
handles.microFocalLength = 0.308;

% Update handles structure
guidata(hObject, handles);

function tagMag_Callback(hObject, eventdata, handles)
% hObject    handle to tagMag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tagMag as text
%        str2double(get(hObject,'String')) returns contents of tagMag as a double
input = str2double(get(hObject,'String'));
handles.Mag = input;

guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function tagMag_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tagMag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function tagf_M_Callback(hObject, eventdata, handles)
% hObject    handle to tagf_M (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tagf_M as text
%        str2double(get(hObject,'String')) returns contents of tagf_M as a double
input = str2double(get(hObject,'String'));
handles.focalLength = input;

guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function tagf_M_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tagf_M (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function tagfnum_M_Callback(hObject, eventdata, handles)
% hObject    handle to tagfnum_M (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tagfnum_M as text
%        str2double(get(hObject,'String')) returns contents of tagfnum_M as a double
input = str2double(get(hObject,'String'));
handles.fNumber = input;

guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function tagfnum_M_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tagfnum_M (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function tagnPixelX_Callback(hObject, eventdata, handles)
% hObject    handle to tagnPixelX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tagnPixelX as text
%        str2double(get(hObject,'String')) returns contents of tagnPixelX as a double
input = str2double(get(hObject,'String'));
handles.nPixelX = input;

guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function tagnPixelX_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tagnPixelX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function tagnPixelY_Callback(hObject, eventdata, handles)
% hObject    handle to tagnPixelY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tagnPixelY as text
%        str2double(get(hObject,'String')) returns contents of tagnPixelY as a double
input = str2double(get(hObject,'String'));
handles.nPixelY = input;

guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function tagnPixelY_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tagnPixelY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in tagSamplingMethod.
function tagSamplingMethod_Callback(hObject, eventdata, handles)
% hObject    handle to tagSamplingMethod (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns tagSamplingMethod contents as cell array
%        contents{get(hObject,'Value')} returns selected item from tagSamplingMethod
types = {'turtle','turtle','lines','spiral'};
handles.samplingMethod = types{ get(hObject,'Value') };
refreshSampling(hObject,handles);

% Image showing uvSampling
imname = ['uv' handles.samplingMethod '.png'];
axes(handles.tagUVimg);
imshow(imname);

% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function tagSamplingMethod_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tagSamplingMethod (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton_genPerspectives.
function pushbutton_genPerspectives_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_genPerspectives (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    fprintf('Generating perspectives for planes:');
    fprintf(' %.2f,', handles.Planes(1:end-1)); fprintf(' %.2f', handles.Planes(end)); fprintf('\n');
    
DLFC_genPerspectives(handles.focalLength, handles.Mag, handles.fNumber, handles.Planes, handles.turtSampling,...
    handles.samplingMethod, handles.nPixelX, handles.nPixelY, handles.mainCal_path, handles.mCal_path, handles.drg_path,...
    handles.sensorSizeX, handles.sensorSizeY, handles.nMicroX, handles.nMicroY, handles.pixelPitch, ...
    handles.microPitch, handles.microFocalLength)


% --- Executes on button press in pushbutton_loaddrg.
function pushbutton_loaddrg_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_loaddrg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if handles.drg_path ~=  0
    directory_name = uigetdir(handles.drg_path,'Select Dragon redist folder...');
    if directory_name ~=  0 % if the user didn't pick a folder
        handles.drg_path = directory_name;
        set(handles.tagTextDRG,'String',directory_name); % update GUI/set to new value
    else
        set(handles.tagTextDRG,'String',handles.drg_path); %set to old value
    end
else
    directory_name = uigetdir(path,'Select Dragon redist folder...');
    if directory_name ~=  0 % if the user didn't pick a folder
        handles.drg_path = directory_name;
        set(handles.tagTextDRG,'String',directory_name); % update GUI/set to new value
    else
        set(handles.tagTextDRG,'String',handles.drg_path); %set to old value
    end
end
guidata(hObject, handles);


% --- Executes on button press in pushbutton_loadMainCal.
function pushbutton_loadMainCal_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_loadMainCal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.mainCal_path ~=  0
    directory_name = uigetdir(handles.mainCal_path,'Select volumetric calibration folder...');
    if directory_name ~=  0 % if the user didn't pick a folder
        handles.mainCal_path = directory_name;
        set(handles.tagTextMainCal,'String',directory_name); % update GUI/set to new value
    else
        set(handles.tagTextMainCal,'String',handles.mainCal_path); %set to old value
    end
else
    directory_name = uigetdir(path,'Select volumetric calibration folder...');
    if directory_name ~=  0 % if the user didn't pick a folder
        handles.mainCal_path = directory_name;
        set(handles.tagTextMainCal,'String',directory_name); % update GUI/set to new value
    else
        set(handles.tagTextMainCal,'String',handles.mainCal_path); %set to old value
    end
end
guidata(hObject, handles);

% --- Executes on button press in pushbutton_loadmCal.
function pushbutton_loadmCal_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_loadmCal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.mCal_path ~=  0
    directory_name = uigetdir(handles.mCal_path,'Select microlens calibration folder...');
    if directory_name ~=  0 % if the user didn't pick a folder
        handles.mCal_path = directory_name;
        set(handles.tagTextmCal,'String',directory_name); % update GUI/set to new value
    else
        set(handles.tagTextmCal,'String',handles.mCal_path); %set to old value
    end
else
    directory_name = uigetdir(path,'Select microlens calibration folder...');
    if directory_name ~=  0 % if the user didn't pick a folder
        handles.mCal_path = directory_name;
        set(handles.tagTextmCal,'String',directory_name); % update GUI/set to new value
    else
        set(handles.tagTextmCal,'String',handles.mCal_path); %set to old value
    end
end
guidata(hObject, handles);


% --- Executes on button press in pushbutton_AssignPoints.
function pushbutton_AssignPoints_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_AssignPoints (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fprintf('Assigning object-space calibration points for plane %.2f\n', handles.assignXYZPlane);

if handles.run2 == 0
    fprintf('Run 1: Assigning object-space calibration points for plane %.2f\n', handles.assignXYZPlane);
    imUsed2 = DLFC_defCBPoints(handles.mainCal_path, handles.assignXYZPlane, handles.run2, handles.topLeftCoord,...
        handles.diagnoseAll, handles.squareSize, handles.nPixelX, handles.nPixelY, ...
        handles.sensorSizeX, handles.sensorSizeY, handles.pixelPitch);
    handles.imUsed2 = imUsed2;
else
    fprintf('Run 2: Assigning object-space calibration points for plane %.2f\n', handles.assignXYZPlane);
    DLFC_defCBPoints(handles.mainCal_path, handles.assignXYZPlane, handles.run2, handles.topLeftCoord,...
        handles.diagnoseAll, handles.squareSize, handles.nPixelX, handles.nPixelY, handles.imUsed2, ...
        handles.sensorSizeX, handles.sensorSizeY, handles.pixelPitch);
end
guidata(hObject, handles);


% --- Executes on button press in pushbutton_CalcCoeffs.
function pushbutton_CalcCoeffs_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_CalcCoeffs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if handles.polyUni == 1
    fprintf('Calculating coefficients of universal polynomial for planes:');
    fprintf(' %.2f,', handles.Planes(1:end-1)); fprintf(' %.2f', handles.Planes(end)); fprintf('\n');
    DLFC_calcCoeffs(handles.mainCal_path, handles.Planes, handles.calOrder)
elseif handles.polyUni == 0
    fprintf('Calculating coefficients of per-perspective polynomial for planes:');
    fprintf(' %.2f,', handles.Planes(1:end-1)); fprintf(' %.2f', handles.Planes(end)); fprintf('\n');
    DLFC_calcCoeffsUV(handles.mainCal_path, handles.Planes, handles.calOrder)
end


% --- Executes on button press in pushbutton_verifyCal.
function pushbutton_verifyCal_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_verifyCal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fprintf('Verifying calibration for planes:');
fprintf(' %.2f,', handles.Planes(1:end-1)); fprintf(' %.2f', handles.Planes(end)); fprintf('\n');

if handles.polyUni == 1
    polyType = 'universal';
elseif handles.polyUni == 0
    polyType = 'uv';
end

DLFC_verifyCal(handles.focalLength, handles.Mag, handles.fNumber, handles.Planes, handles.nPixelX, handles.nPixelY,...
    handles.xMin, handles.xMax, handles.yMin, handles.yMax, polyType, handles.calOrder,...
    handles.samplingMethod, handles.mainCal_path, handles.mCal_path, handles.drg_path,...
    handles.sensorSizeX, handles.sensorSizeY, handles.nMicroX, handles.nMicroY,...
    handles.pixelPitch, handles.microPitch, handles.microFocalLength)


function tagPlanes_Callback(hObject, eventdata, handles)
% hObject    handle to tagPlanes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tagPlanes as text
%        str2double(get(hObject,'String')) returns contents of tagPlanes as a double

planes = strsplit(get(hObject,'String'),',');
for p = 1:length(planes)
    input(p) = str2double(planes{p});
end
handles.Planes = input;
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function tagPlanes_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tagPlanes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function tagAssignXYZPlanes_Callback(hObject, eventdata, handles)
% hObject    handle to tagAssignXYZPlanes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tagAssignXYZPlanes as text
%        str2double(get(hObject,'String')) returns contents of tagAssignXYZPlanes as a double

input = str2double(get(hObject,'String'));
handles.assignXYZPlane = input;

guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function tagAssignXYZPlanes_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tagAssignXYZPlanes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function tagTurtle_Callback(hObject, eventdata, handles)
% hObject    handle to tagTurtle (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tagTurtle as text
%        str2double(get(hObject,'String')) returns contents of tagTurtle as a double
input = str2double(get(hObject,'String'));
handles.turtSampling = input;

guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function tagTurtle_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tagTurtle (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function tagCalOrder_Callback(hObject, eventdata, handles)
% hObject    handle to tagCalOrder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tagCalOrder as text
%        str2double(get(hObject,'String')) returns contents of tagCalOrder as a double
input = str2double(get(hObject,'String'));
handles.calOrder = input;

guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function tagCalOrder_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tagCalOrder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in tagRadioUniversal.
function tagRadioUniversal_Callback(hObject, eventdata, handles)
% hObject    handle to tagRadioUniversal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tagRadioUniversal
input = get(hObject,'Value'); % Get Tag of selected object.
handles.polyUni = input;
handles.polyUV = 0;

% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in tagRadioPerUV.
function tagRadioPerUV_Callback(hObject, eventdata, handles)
% hObject    handle to tagRadioPerUV (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tagRadioPerUV
input = get(hObject,'Value'); % Get Tag of selected object.
handles.polyUni = 0;
handles.polyUV = input;

% Update handles structure
guidata(hObject, handles);



function tagXmin_Callback(hObject, eventdata, handles)
% hObject    handle to tagXmin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tagXmin as text
%        str2double(get(hObject,'String')) returns contents of tagXmin as a double
input = str2double(get(hObject,'String'));
handles.xMin = input;

guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function tagXmin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tagXmin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function tagXmax_Callback(hObject, eventdata, handles)
% hObject    handle to tagXmax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tagXmax as text
%        str2double(get(hObject,'String')) returns contents of tagXmax as a double
input = str2double(get(hObject,'String'));
handles.xMax = input;

guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function tagXmax_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tagXmax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function tagYmin_Callback(hObject, eventdata, handles)
% hObject    handle to tagYmin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tagYmin as text
%        str2double(get(hObject,'String')) returns contents of tagYmin as a double
input = str2double(get(hObject,'String'));
handles.yMin = input;

guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function tagYmin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tagYmin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function tagYmax_Callback(hObject, eventdata, handles)
% hObject    handle to tagYmax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tagYmax as text
%        str2double(get(hObject,'String')) returns contents of tagYmax as a double
input = str2double(get(hObject,'String'));
handles.yMax = input;

guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function tagYmax_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tagYmax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function tagSquareSize_Callback(hObject, eventdata, handles)
% hObject    handle to tagSquareSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tagSquareSize as text
%        str2double(get(hObject,'String')) returns contents of tagSquareSize as a double
input = str2double(get(hObject,'String'));
handles.squareSize = input;

guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function tagSquareSize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tagSquareSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function tagTopLeftCoord_Callback(hObject, eventdata, handles)
% hObject    handle to tagTopLeftCoord (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tagTopLeftCoord as text
%        str2double(get(hObject,'String')) returns contents of tagTopLeftCoord as a double
coord = strsplit(get(hObject,'String'),',');
for p = 1:length(coord)
    input(p) = str2double(coord{p});
end
handles.topLeftCoord = input;
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function tagTopLeftCoord_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tagTopLeftCoord (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in radioDiagnoseAll.
function radioDiagnoseAll_Callback(hObject, eventdata, handles)
% hObject    handle to radioDiagnoseAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radioDiagnoseAll
input = get(hObject,'Value'); % Get Tag of selected object.
handles.diagnoseAll = input;

% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in radioDiagnoseOne.
function radioDiagnoseOne_Callback(hObject, eventdata, handles)
% hObject    handle to radioDiagnoseOne (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radioDiagnoseOne
handles.diagnoseAll = 0;

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in tagRadioRun1.
function tagRadioRun1_Callback(hObject, eventdata, handles)
% hObject    handle to tagRadioRun1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of tagRadioRun1
handles.run2 = 0;

% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in radioRun2.
function radioRun2_Callback(hObject, eventdata, handles)
% hObject    handle to radioRun2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radioRun2
handles.run2 = 1;

% Update handles structure
guidata(hObject, handles);



function tagSensorSizeX_Callback(hObject, eventdata, handles)
% hObject    handle to tagSensorSizeX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tagSensorSizeX as text
%        str2double(get(hObject,'String')) returns contents of tagSensorSizeX as a double
input = str2double(get(hObject,'String'));
handles.sensorSizeX = input;

guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function tagSensorSizeX_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tagSensorSizeX (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function tagSensorSizeY_Callback(hObject, eventdata, handles)
% hObject    handle to tagSensorSizeY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tagSensorSizeY as text
%        str2double(get(hObject,'String')) returns contents of tagSensorSizeY as a double
input = str2double(get(hObject,'String'));
handles.sensorSizeY = input;

guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function tagSensorSizeY_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tagSensorSizeY (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function tagMLx_Callback(hObject, eventdata, handles)
% hObject    handle to tagMLx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tagMLx as text
%        str2double(get(hObject,'String')) returns contents of tagMLx as a double
input = str2double(get(hObject,'String'));
handles.nMicroX = input;

guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function tagMLx_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tagMLx (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function tagMLy_Callback(hObject, eventdata, handles)
% hObject    handle to tagMLy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tagMLy as text
%        str2double(get(hObject,'String')) returns contents of tagMLy as a double
input = str2double(get(hObject,'String'));
handles.nMicroY = input;

guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function tagMLy_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tagMLy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function tagPixelPitch_Callback(hObject, eventdata, handles)
% hObject    handle to tagPixelPitch (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tagPixelPitch as text
%        str2double(get(hObject,'String')) returns contents of tagPixelPitch as a double
input = str2double(get(hObject,'String'));
handles.pixelPitch = input;

guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function tagPixelPitch_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tagPixelPitch (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function tagMLpitch_Callback(hObject, eventdata, handles)
% hObject    handle to tagMLpitch (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tagMLpitch as text
%        str2double(get(hObject,'String')) returns contents of tagMLpitch as a double
input = str2double(get(hObject,'String'));
handles.microPitch = input;

guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function tagMLpitch_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tagMLpitch (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function tagMLfocalLength_Callback(hObject, eventdata, handles)
% hObject    handle to tagMLfocalLength (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of tagMLfocalLength as text
%        str2double(get(hObject,'String')) returns contents of tagMLfocalLength as a double
input = str2double(get(hObject,'String'));
handles.microFocalLength = input;

guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function tagMLfocalLength_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tagMLfocalLength (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Close button not pressed; run button pressed, so let program continue
handles.startProgram2 = true;

% Update handles structure
guidata(hObject, handles);

uiresume(handles.figure1);


% --- Executes on button press in tagSaveHandles.
function tagSaveHandles_Callback(hObject, eventdata, handles)
% hObject    handle to tagSaveHandles (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[filename, pathname] = uiputfile( {'*.cfg','Configuration Files (*.cfg)';}, 'Save');
if filename ~= 0
    saveState(pathname,filename,handles)
end

% --- Executes on button press in tagLoadHandles.
function tagLoadHandles_Callback(hObject, eventdata, handles)
% hObject    handle to tagLoadHandles (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[filename, pathname] = uigetfile({'*.cfg','Configuration Files (*.cfg)';}, 'Select a configuration file to load...');
if filename ~= 0
    handles = loadState(pathname,filename,hObject,handles);
end

% --- Save State Function
function saveState(pathname,filename,handles)

state.v01 = handles.drg_path;
state.v02 = handles.mainCal_path;
state.v03 = handles.mCal_path;
% state.v04 = handles.numThreads;
state.v05 = handles.focalLength;
state.v07 = handles.Mag;
state.v08 = handles.fNumber;
state.v09 = handles.Planes;
state.v10 = handles.nPixelX;
state.v11 = handles.nPixelY;

% state.v12 = handles.turtSampling;
% state.v13 = handles.samplingMethod;
% state.v14 = handles.assignXYZPlane;
% state.v15 = handles.squareSize;
% state.v16 = handles.topLeftCoord;
% state.v17 = handles.diagnoseAll;
% state.v18 = handles.run2;
% state.v19 = handles.calOrder;
% state.v20 = handles.polyUni;
% state.v21 = handles.polyUV;

state.v22 = handles.xMin;
state.v23 = handles.xMax;
state.v24 = handles.yMin;
state.v25 = handles.yMax;

% state.v26 = handles.startProgram2;

state.v27 = handles.sensorSizeX;
state.v28 = handles.sensorSizeY;
state.v29 = handles.nMicroX;
state.v30 = handles.nMicroY;
state.v31 = handles.pixelPitch;
state.v32 = handles.microPitch;
state.v33 = handles.microFocalLength;

save(fullfile(pathname,filename), 'state','-mat');

% --- Load State Function
function handles = loadState(pathname,filename,hObject,handles)
try
    temp = load(fullfile(pathname,filename), '-mat');
    
    handles.drg_path            = temp.state.v01;
    handles.mainCal_path           = temp.state.v02;
    handles.mCal_path            = temp.state.v03;
    
%     handles.numThreads        = temp.state.v04;
    
    handles.focalLength             = temp.state.v05;
    handles.Mag          = temp.state.v07;
    handles.fNumber          = temp.state.v08;
    handles.Planes         = temp.state.v09;
    handles.nPixelX         = temp.state.v10;
    handles.nPixelY        = temp.state.v11;
    
%     handles.turtSampling       = temp.state.v12;
%     handles.samplingMethod          = temp.state.v13;
%     handles.assignXYZPlane           = temp.state.v14;
%     handles.squareSize           = temp.state.v15;
%     handles.topLeftCoord  = temp.state.v16;
%     handles.diagnoseAll            = temp.state.v17;
%     handles.run2            = temp.state.v18;
%     handles.calOrder   = temp.state.v19;
%     handles.polyUni          = temp.state.v20;
%     handles.polyUV          = temp.state.v21;
    
    handles.xMin          = temp.state.v22;
    handles.xMax          = temp.state.v23;
    handles.yMin          = temp.state.v24;
    handles.yMax          = temp.state.v25;
    
%     handles.startProgram2          = temp.state.v26;
    
    handles.sensorSizeX          = temp.state.v27;
    handles.sensorSizeY          = temp.state.v28;
    handles.nMicroX          = temp.state.v29;
    handles.nMicroY          = temp.state.v30;
    handles.pixelPitch          = temp.state.v31;
    handles.microPitch          = temp.state.v32;
    handles.microFocalLength          = temp.state.v33;

    % Set text inputs
    set(handles.tagTextDRG, 'String', handles.drg_path);
    set(handles.tagTextMainCal, 'String', handles.mainCal_path);
    set(handles.tagTextmCal, 'String', handles.mCal_path);

    set(handles.tagf_M, 'String', handles.focalLength);
    set(handles.tagMag, 'String', handles.Mag);
    set(handles.tagfnum_M, 'String', handles.fNumber);
    set(handles.tagnPixelX, 'String', handles.nPixelX);
    set(handles.tagnPixelY, 'String', handles.nPixelY);
    set(handles.tagPlanes, 'String', num2str(handles.Planes));

    set(handles.tagSensorSizeX, 'String', handles.sensorSizeX);
    set(handles.tagSensorSizeY, 'String', handles.sensorSizeY);
    set(handles.tagMLx, 'String', handles.nMicroX);
    set(handles.tagMLy, 'String', handles.nMicroY);
    set(handles.tagPixelPitch, 'String', handles.pixelPitch);
    set(handles.tagMLpitch, 'String', handles.microPitch);
    set(handles.tagMLfocalLength, 'String', handles.microFocalLength);
   
    set(handles.tagXmin, 'String', handles.xMin);
    set(handles.tagXmax, 'String', handles.xMax);
    set(handles.tagYmin, 'String', handles.yMin);
    set(handles.tagYmax, 'String', handles.yMax);

    
catch generror2
    %load failed
    warning('Loading of previous configuration settings failed. Resetting to default values...');
    handles = setDefaults(handles);
end

% Update handles structure
guidata(hObject, handles);

function refreshSampling(hObject,handles)

if strcmp(handles.samplingMethod, 'turtle') == 1
    set(handles.tagTurtle,'Enable','on');
    set(handles.tagRadioPerUV,'Enable','off');
else
    set(handles.tagTurtle,'Enable','off');
    set(handles.tagRadioPerUV,'Enable','on');
end
