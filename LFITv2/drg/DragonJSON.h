#ifndef DRAGON_JSON_H
#define DRAGON_JSON_H

#define LINESIZE 512

enum JSON_type {
	JSON_UNDEFINED = 0,
	JSON_OBJECT    = 1,
	JSON_ARRAY     = 2,
	JSON_STRING    = 3,
	JSON_PRIMATIVE = 4
};

struct JSON {
	JSON *next;  // next item in the linked list of JSON objects
	JSON *child; // An array or object item with have a child pointer pointing to a chain of items in the array/object
	char *name;  // name of object
	JSON_type type;
	int nChildren;

	union {
		float fval;
		char *sval;
	} val;
};

// Create JSON root object
JSON* JSON_create(const char* data);
void  JSON_destroy(JSON *obj);

// Get JSON item
JSON*  JSON_getItem(JSON* root, const char* key);
JSON** JSON_getArrayItem(JSON* root, const char* key);

int    JSON_getArrayLength(JSON* root, const char* key);

// Get Value
bool   JSON_getBool(JSON* root, const char* key);

char*  JSON_getString(JSON* root, const char* key);
char** JSON_getArrayString(JSON* root, const char* key);
char*  JSON_getArrayStringElement(JSON* root, const char* key, int idx);

int    JSON_getInt(JSON* root, const char* key);
int*   JSON_getArrayInt(JSON* root, const char* key);
int    JSON_getArrayIntElement(JSON* root, const char* key, int idx);

float  JSON_getFloat(JSON* root, const char* key);
float* JSON_getArrayFloat(JSON* root, const char* key);
float  JSON_getArrayFloatElement(JSON* root, const char* key, int idx);

JSON* JSON_readFile(char *fileName);

#endif

#ifdef DRAGON_JSON_IMPLEMENTATION

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const char* eat_whitespace(const char *in)
{
	if (!in) return NULL;
	while (*in && (unsigned char)*in <= 32) {
		in++;
		if (*in == '#') {  // treat comments as whitespace
			in++;
			while (*in && (unsigned char)*in != 10 && (unsigned char)*in != 13)
				in++;
		}
	}
	return in;
}

static int JSON_strcasecmp(const char* s1, const char* s2) 
{
	if (s1 && s2) 
	{
#if defined(_WIN32)
		return _stricmp(s1, s2);
#else
		return strcasecmp( s1, s2 );
#endif
	}
	else 
	{
		if (s1 < s2)
			return -1; /* s1 is null, s2 is not */
		else if (s1 == s2)
			return 0; /* both are null */
		else
			return 1; /* s2 is nul	s1 is not */
	}
}

const char* parse_string(JSON *item, const char* str)
{
	if (*str != '\"') return NULL;

	const char* ptr = str + 1;
	
	// Get string length
	int length = 0;
	while (*ptr != '\"' && *ptr && ++length)
		if (*ptr++ == '\\') ptr++;

	// Allocate output string 
	char* out = (char*)malloc(length+1);
	if (!out) return NULL;

	// Populate string
	ptr = str + 1;
	char* ptr2 = out;

	while (*ptr != '\"' && *ptr)
	{
		if (*ptr != '\\')
			*ptr2++ = *ptr++;
		else 
		{
			ptr++;
			switch(*ptr)
			{
				case 'b':
				{
					*ptr2++ = '\b';
				} break;
				case 'f':
				{
					*ptr2++ = '\f';
				} break;
				case 'n':
				{
					*ptr2++ = '\n';
				} break;
				case 'r':
				{
					*ptr2++ = '\r';
				} break;
				case 't':
				{
					*ptr2++ = '\t';
				} break;
				default:
				{
					*ptr2++ = *ptr;
				} break;
			}
		}
	}
	*ptr2 = 0;
	if (*ptr == '\"') ptr++;
	item->val.sval = out;
	item->type = JSON_STRING;
	return ptr;
}

const char* parse_primative(JSON *item, const char* num)
{
	item->type = JSON_PRIMATIVE;

	if (!strncmp(num, "null", 4))
	{
		return num + 4;
	}
	if (!strncmp(num, "false", 5))
	{
		item->val.fval = 0;
		return num + 5;
	}
	if (!strncmp(num, "true", 4))
	{
		item->val.fval = 1;
		return num + 4;
	}

	char *endPtr;
	float n = strtof(num, &endPtr);

	if (endPtr != num) 
	{
		item->val.fval = n;
		return endPtr;
	}
	else
	{
		return NULL;
	}
}

const char* parse_value(JSON* item, const char* value);
const char* parse_object(JSON *item, const char* value);
const char* parse_array(JSON *item, const char* value);

const char* parse_object(JSON *item, const char* value)
{
	// Set initial JSON object properties
	item->type = JSON_OBJECT;
	value = eat_whitespace(value + 1);
	if (*value == '}') return value + 1; // empty object

	// Create child object
	JSON *child;
	item->child = child = (JSON*)calloc(sizeof(JSON),1);
	if (!item->child) return NULL;

	// Get key name of child
	value = eat_whitespace(parse_string(child,eat_whitespace(value)));
	if (!value) return NULL;
	child->name = child->val.sval;
	child->val.sval = 0;

	if (*value != ':') return NULL;

	// Get value for child object
	value = eat_whitespace(parse_value(child,eat_whitespace(value + 1)));
	if (!value) return NULL;

	// Set root object number of children to 1
	item->nChildren = 1;

	// Get remaining children
	while (*value == ',') 
	{
		// Create new item
		JSON *newItem = (JSON*)calloc(sizeof(JSON),1);
		if (!newItem) return NULL;

		child->next = newItem;
		child = newItem;

		value = eat_whitespace(parse_string(child,eat_whitespace(value + 1)));
		if (!value) return NULL;
		child->name = child->val.sval;
		child->val.sval = 0;

		if (*value != ':') return NULL;

		value = eat_whitespace(parse_value(child,eat_whitespace(value + 1)));
		if (!value) return NULL;
		item->nChildren++;
	}

	if (*value == '}') return value + 1; // end of object
	return NULL;
}

const char* parse_value(JSON* item, const char* value)
{
	switch (*value) 
	{
		case 'n':
		case 't':
		case 'f':
		case '-':
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			return parse_primative(item,value);
		case '\"':	
			return parse_string(item,value);
		case '[':
			return parse_array(item,value);
		case '{':
			return parse_object(item,value);
		default:
			break;	
	}
	return NULL;
}

const char* parse_array(JSON *item, const char* value)
{
	item->type = JSON_ARRAY;
	value = eat_whitespace(value + 1);
	if (*value == ']') return value + 1;

	JSON *child;
	item->child = child = (JSON*)calloc(sizeof(JSON),1);
	if (!item->child) return NULL;

	value = eat_whitespace(parse_value(child,eat_whitespace(value)));
	if (!value) return NULL;
	item->nChildren = 1;

	while (*value == ',')
	{
		JSON *newItem = (JSON*)calloc(sizeof(JSON),1);
		if (!newItem) return NULL;

		child->next = newItem;
		child = newItem;

		value = eat_whitespace(parse_value(child,eat_whitespace(value + 1)));
		if (!value) return NULL;
		item->nChildren++;
	}

	if (*value == ']') return value + 1; // end of object
	return NULL;
}

JSON* JSON_readFile(char *fileName)
{
	// Read file into buffer
	FILE* fileID = fopen(fileName,"r");
	if (!fileID) { return NULL; };

	fseek(fileID, 0, SEEK_END);
	int fileLength = (int)ftell(fileID);
	fseek(fileID, 0, SEEK_SET);

	char *buffer = (char*)malloc(fileLength);
	fread(buffer, 1, fileLength, fileID);
	fclose(fileID);

	JSON* root = JSON_create(buffer);
	free(buffer);

	if (!root) return NULL;
	return root;
}

// Create JSON root object
JSON* JSON_create(const char* data)
{
	// Initialize JSON struct
	JSON* root = (JSON*)calloc(sizeof(JSON),1);

	// Populate root object
	data = parse_value(root, eat_whitespace(data));

	if (!data)
	{
		JSON_destroy(root);
		return NULL;
	}

	return root;
}

void JSON_destroy(JSON *obj)
{
	JSON *next;
	while (obj) {
		next = obj->next;
		if (obj->child) JSON_destroy(obj->child);
		if (obj->type == JSON_STRING) free(obj->val.sval);
		if (obj->name) free(obj->name);
		free(obj);
		obj = next;
	}
}

JSON* JSON_getItem(JSON* root, const char* key)
{
	JSON *child = root->child;
	while (child && JSON_strcasecmp(child->name, key))
		child = child->next;
	return child;
}

JSON** JSON_getArrayItem(JSON* root, const char* key)
{
	JSON** result = NULL;
	root = JSON_getItem(root, key);
	if (root && root->type == JSON_ARRAY) {
		int nChildren = root->nChildren;
		result = (JSON**)malloc(nChildren*sizeof(JSON*));

		JSON *child = root->child;
		for (int i = 0; i < nChildren; ++i) {
			result[i] = child;
			child = child->next;
		}
	}
	else if (root && root->type == JSON_OBJECT) {
		result = (JSON**)malloc(sizeof(JSON*));
		result[0] = root;
	}
	return result;
}

int JSON_getArrayLength(JSON* root, const char* key)
{
	root = JSON_getItem(root, key);
	if (root && root->type == JSON_ARRAY)
		return root->nChildren;
	return (root) ? 1 : 0;
}

bool JSON_getBool(JSON* root, const char* key)
{
	root = JSON_getItem(root, key);
	return (root) ? (int)root->val.fval : 0;
}

char* JSON_getString(JSON* root, const char* key)
{
	char* result = NULL;
	root = JSON_getItem(root, key);
	if (root) {
		result = (char*)malloc(LINESIZE*sizeof(char));
		strcpy(result, root->val.sval);
	}
	return result;
}

char** JSON_getArrayString(JSON* root, const char* key)
{
	char** result = NULL;
	root = JSON_getItem(root, key);
	if (root && root->type == JSON_ARRAY) {
		int nChildren = root->nChildren;
		result = (char**)malloc(nChildren*sizeof(char*));

		JSON *child = root->child;
		for (int i = 0; i < nChildren; ++i) {
			result[i] = child->val.sval;
			child = child->next;
		}
	}
	else if (root && root->type == JSON_STRING) {
		result = (char**)malloc(sizeof(char*));
		result[0] = root->val.sval;
	}
	return result;
}

char* JSON_getArrayStringElement(JSON* root, const char* key, int idx)
{
	char** array = JSON_getArrayString(root,key);
	return array[idx];
}

int JSON_getInt(JSON* root, const char* key)
{
	root = JSON_getItem(root, key);
	return (root) ? (int)root->val.fval : 0;
}

int* JSON_getArrayInt(JSON* root, const char* key)
{
	int* result = NULL;
	root = JSON_getItem(root, key);
	if (root && root->type == JSON_ARRAY) {
		int nChildren = root->nChildren;
		result = (int*)malloc(nChildren*sizeof(int));

		JSON *child = root->child;
		for (int i = 0; i < nChildren; ++i) {
			result[i] = (int)child->val.fval;
			child = child->next;
		}
	}
	else if (root && root->type == JSON_PRIMATIVE) {
		result = (int*)malloc(sizeof(int));
		result[0] = (int)root->val.fval;
	}
	return result;
}

int JSON_getArrayIntElement(JSON* root, const char* key, int idx)
{
	int* array = JSON_getArrayInt(root,key);
	return array[idx];
}

float JSON_getFloat(JSON* root, const char* key)
{
	root = JSON_getItem(root, key);
	return (root) ? root->val.fval : 0.f;
}

float* JSON_getArrayFloat(JSON* root, const char* key)
{
	float* result = NULL;
	root = JSON_getItem(root, key);
	if (root && root->type == JSON_ARRAY) {
		int nChildren = root->nChildren;
		result = (float*)malloc(nChildren*sizeof(float));

		JSON *child = root->child;
		for (int i = 0; i < nChildren; ++i) {
			result[i] = child->val.fval;
			child = child->next;
		}
	}
	else if (root && root->type == JSON_PRIMATIVE) {
		result = (float*)malloc(sizeof(float));
		result[0] = root->val.fval;
	}
	return result;
}

float JSON_getArrayFloatElement(JSON* root, const char* key, int idx)
{
	float* array = JSON_getArrayFloat(root,key);
	return array[idx];
}

#endif
