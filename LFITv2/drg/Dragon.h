/*
File: Dragon.h
Programmer: Tim Fahringer
Date Modified: 8-5-2015
Purpose: Defines external API for Dragon .dll
*/


#define _CRT_SECURE_NO_WARNINGS

#ifndef DRAGON_API_H
#define DRAGON_API_H

#include <stdio.h>   // fprintf
#include <stdlib.h>  // calloc, etc.
#include <stdarg.h>  // vararg
#include <errno.h>   // error numbers
#include <math.h>    // sin/cos/tan
#include <omp.h>     // openMP functions
#include <time.h>    // rand seed

#ifdef WITH_CUDA
#  include <cuda.h>
#endif

//------------------------------------------------------------------------------
// Defines
//------------------------------------------------------------------------------
#ifdef __cplusplus
extern "C" {
#endif

#ifndef API
#  ifdef DRAGON_SIMPLIFY_HEADERS
#    define API
#  elif defined _WIN32
#    define API __declspec(dllexport)
#  elif defined __GNUC__
#    define API __attribute__ ((visibility("default")))
#  else
#    define API
#  endif
#endif

#define MAX_STRING_LENGTH 512
#define MAX_DIMS_ARRAY 5
#define PI 3.14159265359
#define M_E 2.71828182845904523536
#define NUM_COEFFS_4ORDER 126

//------------------------------------------------------------------------------
// Data type definitions
//------------------------------------------------------------------------------
typedef char s8;
typedef unsigned char u8;
typedef short s16;
typedef unsigned short u16;
typedef int s32;
typedef unsigned int u32;
typedef long long int s64;
typedef unsigned long long int u64;
typedef float r32;
typedef double r64;

//------------------------------------------------------------------------------
// Enumerations
//------------------------------------------------------------------------------
enum ErrorCode {FAILURE = 0, SUCCESS = 1, IO = 2, ALLOC = 3, OTHER = 99};  // to be replaced with errno
enum LogLevel {UNDEF = 0, ERROR = 1, WARNING = 2, INFO = 3, DEBUG = 4, TRACE = 5};
enum ParticleGeneration {RANDOM = 0};
enum ReconstructionType {INTEGRAL = 0, FILTERED = 1, MART = 2, MULTIPLICATIVE = 3, AART = 4, ART = 5, SART = 6, ASART = 7};
enum CalibrationOrder { ORDER2 = 0, ORDER3 = 1, ORDER4 = 2, ORDER2UV = 3, ORDER3UV = 4, ORDER4UV = 5 };
enum MicrolensArrayType {RECTANGULAR = 1, HEXAGONAL = 2};
enum WindowWeights {UNIFORM = 0, GAUSSIAN = 1};
enum WindowDeformation {NONE = 0, BILINEAR = 1, SINC7 = 2, SINC11 = 3};
enum BosRadianceType {U_VECTORS = 0, V_VECTORS = 1, MAGNITUDE = 2};
enum VolumeMask {STATIC_ONLY = 1, STATIC_AND_DYNAMIC = 2};
enum ApertureSampling {STANDARD = 0, LINES = 1, SPIRAL = 2, USER_DEFINED = 9};
enum RegularizationType {TV_YANG = 1};

//------------------------------------------------------------------------------
// Structure Definitions
//------------------------------------------------------------------------------
typedef struct VolumeSettings_t {
	r32 *xArray, *yArray, *zArray;
	s32 size[3]; // voxel count in x,y,z
} VolumeSettings;

typedef struct CameraSettings_t {
	r32 s_i;         // image distance
	r32 s_o;         // object distance
	r32 fnum_m;      // f-number of objective lens
	r32 f_m;         // focal length of objective lens
	r32 p_m;         // pitch of objective lens
	r32 p_p;         // pitch of pixels
	r32 p_l;         // pitch of microlenses (RECT)
	r32 ml_width;    // pitch of microlenses (HEXA) flat to flat
	r32 ml_height;   // pitch of microlenses (HEXA) point to point
	r32 f_l;         // focal length of microlenses
	r32 M;           // magnification
	r32 p_ccd_x;     // physical size of CCD in x-dir
	r32 p_ccd_y;     // physical size of CCD in y-dir
	s32 *sensorSize; // pixel count in x,y
	s32 *arraySize;  // microlens count in x,y
	s32 arrayType;   // type of microlens array
	s32 numPix;      // number of pixels behind a microlens (1D)
} CameraSettings;

typedef struct SimulationSettings_t {
	s32 nRays;      // number of rays for ray-tracing
	r32 *camAngles; // camera rotation angles in x,y pairs
	s32 overwrite;  // overwrite existing files?
} SimulationSettings;

typedef struct LightFieldSettings_t {
	r32 stSamplingFactor;     // sensor sampling factor for perspective generation
	r32 uvSamplingFactor;     // aperture sampling factor for perspective generation
	s32 uvSamplingScheme;     // aperture sampling scheme
	s32 enforceNonNegative;   // enforce non-negative radiance values
	s32 interpolationScheme;  // 4D interpolation scheme
	s32 useSlidingMeanFilter; // use sliding mean filter on perspective views?
} LightFieldSettings;

typedef struct ReconstructionSettings_t {
	s32 algorithm;           // reconstruction algorithm (see ReconstructionType enum)
	s32 nIterations;         // number of iterations to run (iterative algorithms only)
	s32 outputEveryIter;     // output after every N iterations (iterative algorithms only)
	r32 relaxationParameter; // iterative relaxation parameter (iterative algorithms only)
	r32 smoothingParameter;  // iterative smoothing parameter (iterative algorithms only)
	r32 voxelCutoff;         // when to stop iterating on a voxel i.e. skip it (iterative algorithms only)
	r32 voxelDefault;        // default value to assign voxels (currently UBOST only)
	r32 *voxelLimits;        // valid voxel range
	s32 maskingScheme;       // which masking scheme to apply to volume (see VolumeMask enum; iterative algorithms only)
	s32 outputMasks;         // output masks for inspection?
	s32 enforceNonNegative;  // enforce non-negative voxel values?
	r32 intensityThreshold;  // (FR) threshold above which a projection is valid
	r32 filterThreshold;     // (FR) percent of valid vectors needed for a voxel to be valid
} ReconstructionSettings;

typedef struct RegularizationSettings_t {
	s32 algorithm;   // regularization algorithm
	s32 nIterations; // number of regularization iterations
	r32 *penalties;  // reguarization penalties in x,y,z
} RegularizationSettings;

typedef struct PivSettings_t {
	s32 useMask;               // use a masking image/volume?
	s32 useCrop;               // use crop limits?
	s32 weightingFunction;     // window weighting function for cross-correlation (see WindowWeights enum)
	s32 deformationScheme;     // interpolation scheme used in image/volume deformation (see WindowDeformation enum)
	r32 validationThreshold;   // threshold of normalized median filter
	s32 nValidationIterations; // number of passes in vector validation and replacement
	s32 nPasses;
	s32 *windowSizesX;
	s32 *windowSizesY;
	s32 *windowSizesZ;
	s32 *windowOffsetsX;
	s32 *windowOffsetsY;
	s32 *windowOffsetsZ;
	s32 *cropX;                // cropped x range
	s32 *cropY;                // cropped y range
	s32 *cropZ;                // cropped z range
	r32 *limitsU;              // valid u velocity range
	r32 *limitsV;              // valid v velocity range
	r32 *limitsW;              // valid w velocity range
} PivSettings;

typedef struct OpticalFlowSettings_t {
	s32 useCrop;           // use crop limits?
	s32 algorithm;         // optical flow algorithm
	s32 weightingFunction; // window weighting function (see WindowWeights enum)
	r32 pyramidScale;      // image scale (<1) to build pyramids for each image
	s32 pyramidDepth;      // number of pyramid layers including the initial image
	s32 windowSize;        // averaging window size
	s32 nIterations;       // number of iterations PER pyramid level
	s32 polyN;             // size of pixel neighborhood used to find polynomial expansion
	r32 polyS;             // standard deviation of Gaussian smoothing
	s32 *cropX;            // cropped x range
	s32 *cropY;            // cropped y range
	s32 *cropZ;            // cropped z range
} OpticalFlowSettings;

typedef struct FilepathSettings_t {
	char *mcal;
	char *lfcal;
	char *bgcal;
	char *uv;
	char *images;
	char *volumes;
	char *vectors;
	char *iThresh;
	char *fThresh;
	char *imageMask;
	char *volumeMask;
} FilepathSettings;

typedef struct ProcessingSettings_t {
	char *logFile;  // log filename
	s32 logLevel;   // log detail level
	s32 maxThreads; // maximum number of execution threads
	s32 useCuda;    // parallelize using CUDA?
	u32 calorder;   // which calibration map to use based on enum: CalibrationOrder
} ProcessingSettings;

typedef struct DragonSettings_t {
	VolumeSettings volume;
	CameraSettings *camera;  s32 nCameras;
	SimulationSettings simulation;
	LightFieldSettings lightfield;
	ReconstructionSettings reconstruction;
	RegularizationSettings regularization;
	PivSettings piv;
	OpticalFlowSettings of;
	FilepathSettings filepath;
	ProcessingSettings processing;
} DragonSettings;

typedef struct LightField_t {
	s32 camID;                              // associated camera number
	s32 isRadianceBuilt;                    // flag for if radiance has been calculated
	u32 dimX, dimY, dimU, dimV, dimSS;      // dimensions of matrices
	r32 theta_U, theta_L;                   // orientation of microlens array
	r32 *xArray, *yArray, *uArray, *vArray; // light field coordinate matrices
	r32 *radArray;                          // light field radiance array
	r32 *threshArray;                       // variable intensity threshold array (FR)
	s32 nImages;                            // number of raw images
} LightField;

typedef struct VolumeCalibration_t {
	r32 xCoeff[NUM_COEFFS_4ORDER]; // X
	r32 yCoeff[NUM_COEFFS_4ORDER]; // Y
} VolumeCalibration;

typedef struct ApertureFunc_t {
	s32 numUV;   // total number of u/v samples
	r32 *uPrime; // vector of u sample locations
	r32 *vPrime; // vector of v sample locations
} ApertureFunc;

typedef struct VolumePosition_t {
	r32 s_o_prime;
	r32 s_i_prime;
	r32 M_prime;
	r32 alpha;
} VolumePosition;

//------------------------------------------------------------------------------
// API Function Definitions
//------------------------------------------------------------------------------
API void DRG_quit(void);

// Settings
API s32 DRG_initFromJSON(char *settingsFileName);

// File IO
API s32 DRG_loadImage(char *imageFileName, r32 **imageData, s32 *width, s32 *length);
API s32 DRG_loadMicrolensCalibration(char *fileName, r32 **cLocX, r32 **cLocY, s32 *nMicroX, s32 *nMicroY);
API s32 DRG_loadApertureSamples(char *fileName, r32 **u, r32 **v, s32 *numUV);
API s32 DRG_saveApertureSamples(char *fileName);

// Light field rendering
API s32 DRG_buildRadiance(s32 nMicroX, s32 nMicroY, r32 *cLocX, r32 *cLocY, r32 *imageData, s32 nImages, r32 *threshImage);
API s32 DRG_buildRadianceFromBOSVectors(char *vectorDir, r32 *xVec, r32 *yVec, r32 *uVec, s32 numU, r32 *vVec, s32 numV, s32 bosRadianceType);

// Refocusing based on microlens coordinates with alpha scaling built in
API s32 DRG_refocusLightFieldToImage(r32 alpha, s32 nVoxelsX, s32 nVoxelsY, r32 *refImage);
API s32 DRG_refocusLightFieldToFocalStack(r32 *alphaVector, s32 nAlphas, s32 nVoxelsX, s32 nVoxelsY, r32 *focalStack);
API s32 DRG_reconstructLightFieldToImage(r32 zPosition, r32 *xVector, s32 nVoxelsX, r32 *yVector, s32 nVoxelsY, r32 *filterThreshold, s32 perUV, s32 camNumber, r32 *refImage);

// Perspective view generation
API s32 DRG_generateSinglePerspectiveView(r32 *xPrimeVector, s32 nVoxelsX, r32 *yPrimeVector, s32 nVoxelsY, r32 uPrime, r32 vPrime, r32 *perImage);
API s32 DRG_generatePerspectiveViews(r32 *xPrimeVector, s32 nVoxelsX, r32 *yPrimeVector, s32 nVoxelsY, r32 *uPrimeVector, r32 *vPrimeVector, s32 numUV, r32 *perImageSweep);


#ifdef __cplusplus
}
#endif

#endif
