function [  ] = DLFC_calcCoeffs(mainDir, planes, order)

%% Filepaths
saveDir = [mainDir, '\Results\'];

%% Read in data
xAct = [];
yAct = [];
zAct = [];
sDot = [];
tDot = [];
uDot = [];
vDot = [];

for i = 1:length(planes)

    fileID = fopen([saveDir num2str(planes(i),'%+g') '.drg-dots'],'r');
    vars = fscanf(fileID,'%f,%f,%f,%f,%f,%f,%f',[7 inf]);
    fclose(fileID);

    for j = 1:size(vars,2)
        if ~isinf(vars(3,j))
            sDot = [sDot; vars(1,j)];
            tDot = [tDot; vars(2,j)];

            xAct = [xAct; vars(3,j)];
            yAct = [yAct; vars(4,j)];
            zAct = [zAct; vars(5,j)];

            uDot = [uDot; vars(6,j)];
            vDot = [vDot; vars(7,j)];
        end
    end
end
    
figure; scatter3( xAct,yAct,zAct );

% Transformation matrix
A = zeros(length(xAct),20);

switch order
	case 2
        for idx = 1:length(xAct)
            x = xAct(idx);
            y = yAct(idx);
            z = zAct(idx);
            u = uDot(idx);
            v = vDot(idx);

            A(idx,1)  = 1;
            A(idx,2)  = x;
            A(idx,3)  = y;
            A(idx,4)  = z;
            A(idx,5)  = u;
            A(idx,6)  = v;
            A(idx,7)  = x * x;
            A(idx,8)  = x * y;
            A(idx,9)  = x * z;
            A(idx,10) = x * u;
            A(idx,11) = x * v;
            A(idx,12) = y * y;
            A(idx,13) = y * z;
            A(idx,14) = y * u;
            A(idx,15) = y * v;
            A(idx,16) = z * z;
            A(idx,17) = z * u;
            A(idx,18) = z * v;
            A(idx,19) = u * u;
            A(idx,20) = u * v;
            A(idx,21) = v * v;
        end
        
	case 3
        for idx = 1:length(xAct)
            x = xAct(idx);
            y = yAct(idx);
            z = zAct(idx);
            u = uDot(idx);
            v = vDot(idx);

            A(idx,1)  = 1;
            A(idx,2)  = x;
            A(idx,3)  = y;
            A(idx,4)  = z;
            A(idx,5)  = u;
            A(idx,6)  = v;
            A(idx,7)  = x * x;
            A(idx,8)  = x * y;
            A(idx,9)  = x * z;
            A(idx,10) = x * u;
            A(idx,11) = x * v;
            A(idx,12) = y * y;
            A(idx,13) = y * z;
            A(idx,14) = y * u;
            A(idx,15) = y * v;
            A(idx,16) = z * z;
            A(idx,17) = z * u;
            A(idx,18) = z * v;
            A(idx,19) = u * u;
            A(idx,20) = u * v;
            A(idx,21) = v * v;
            A(idx,22) = x * x * x;
            A(idx,23) = x * x * y;
            A(idx,24) = x * x * z;
            A(idx,25) = x * x * u;
            A(idx,26) = x * x * v;
            A(idx,27) = x * y * y;
            A(idx,28) = x * y * z;
            A(idx,29) = x * y * u;
            A(idx,30) = x * y * v;
            A(idx,31) = x * z * z;
            A(idx,32) = x * z * u;
            A(idx,33) = x * z * v;
            A(idx,34) = x * u * u;
            A(idx,35) = x * u * v;
            A(idx,36) = x * v * v;
            A(idx,37) = y * y * y;
            A(idx,38) = y * y * z;
            A(idx,39) = y * z * z;
            A(idx,40) = y * z * u;
            A(idx,41) = y * z * v;
            A(idx,42) = y * y * u;
            A(idx,43) = y * u * u;
            A(idx,44) = y * u * v;
            A(idx,45) = y * y * v;
            A(idx,46) = y * v * v;
            A(idx,47) = z * z * z;
            A(idx,48) = z * z * u;
            A(idx,49) = z * u * u;
            A(idx,50) = z * u * v;
            A(idx,51) = z * z * v;
            A(idx,52) = z * v * v;
            A(idx,53) = u * u * u;
            A(idx,54) = u * u * v;
            A(idx,55) = u * v * v;
            A(idx,56) = v * v * v;
        end
         
	case 4
        for idx = 1:length(xAct)
            x = xAct(idx);
            y = yAct(idx);
            z = zAct(idx);
            u = uDot(idx);
            v = vDot(idx);

            A(idx,1)  = 1;
            A(idx,2)  = x;
            A(idx,3)  = y;
            A(idx,4)  = z;
            A(idx,5)  = u;
            A(idx,6)  = v;
            A(idx,7)  = x * x;
            A(idx,8)  = x * y;
            A(idx,9)  = x * z;
            A(idx,10) = x * u;
            A(idx,11) = x * v;
            A(idx,12) = y * y;
            A(idx,13) = y * z;
            A(idx,14) = y * u;
            A(idx,15) = y * v;
            A(idx,16) = z * z;
            A(idx,17) = z * u;
            A(idx,18) = z * v;
            A(idx,19) = u * u;
            A(idx,20) = u * v;
            A(idx,21) = v * v;
            A(idx,22) = x * x * x;
            A(idx,23) = x * x * y;
            A(idx,24) = x * x * z;
            A(idx,25) = x * x * u;
            A(idx,26) = x * x * v;
            A(idx,27) = x * y * y;
            A(idx,28) = x * y * z;
            A(idx,29) = x * y * u;
            A(idx,30) = x * y * v;
            A(idx,31) = x * z * z;
            A(idx,32) = x * z * u;
            A(idx,33) = x * z * v;
            A(idx,34) = x * u * u;
            A(idx,35) = x * u * v;
            A(idx,36) = x * v * v;
            A(idx,37) = y * y * y;
            A(idx,38) = y * y * z;
            A(idx,39) = y * z * z;
            A(idx,40) = y * z * u;
            A(idx,41) = y * z * v;
            A(idx,42) = y * y * u;
            A(idx,43) = y * u * u;
            A(idx,44) = y * u * v;
            A(idx,45) = y * y * v;
            A(idx,46) = y * v * v;
            A(idx,47) = z * z * z;
            A(idx,48) = z * z * u;
            A(idx,49) = z * u * u;
            A(idx,50) = z * u * v;
            A(idx,51) = z * z * v;
            A(idx,52) = z * v * v;
            A(idx,53) = u * u * u;
            A(idx,54) = u * u * v;
            A(idx,55) = u * v * v;
            A(idx,56) = v * v * v;
            A(idx,57) = x * x * x * x;
            A(idx,58) = x * x * x * y;
            A(idx,59) = x * x * x * z;
            A(idx,60) = x * x * x * u;
            A(idx,61) = x * x * x * v;
            A(idx,62) = x * x * y * y;
            A(idx,63) = x * x * y * z;
            A(idx,64) = x * x * y * u;
            A(idx,65) = x * x * y * v;
            A(idx,66) = x * x * z * z;
            A(idx,67) = x * x * z * u;
            A(idx,68) = x * x * z * v;
            A(idx,69) = x * x * u * u;
            A(idx,70) = x * x * u * v;
            A(idx,71) = x * x * v * v;
            A(idx,72) = x * y * y * y;
            A(idx,73) = x * y * y * z;        
            A(idx,74) = x * y * y * u;
            A(idx,75) = x * y * y * v;    
            A(idx,76) = x * y * z * z;
            A(idx,77) = x * y * z * u;    
            A(idx,78) = x * y * z * v;    
            A(idx,79) = x * y * u * u;    
            A(idx,80) = x * y * u * v;  
            A(idx,81) = x * y * v * v;
            A(idx,82) = x * z * z * z;
            A(idx,83) = x * z * z * u;    
            A(idx,84) = x * z * z * v;    
            A(idx,85) = x * z * u * u;
            A(idx,86) = x * z * u * v;   
            A(idx,87) = x * z * v * v;
            A(idx,88) = x * u * u * u;
            A(idx,89) = x * u * u * v;
            A(idx,90) = x * u * v * v;
            A(idx,91) = x * v * v * v;
            A(idx,92) = y * y * y * y;
            A(idx,93) = y * y * y * z;    
            A(idx,94) = y * y * y * u;
            A(idx,95) = y * y * y * v;    
            A(idx,96) = y * y * z * z;
            A(idx,97) = y * y * z * u;
            A(idx,98) = y * y * z * v;
            A(idx,99) = y * y * u * u;
            A(idx,100) = y * y * u * v;
            A(idx,101) = y * y * v * v;
            A(idx,102) = y * z * z * z;
            A(idx,103) = y * z * z * u;
            A(idx,104) = y * z * z * v;
            A(idx,105) = y * z * u * u;
            A(idx,106) = y * z * u * v;
            A(idx,107) = y * z * v * v;
            A(idx,108) = y * u * u * u;
            A(idx,109) = y * u * u * v;
            A(idx,110) = y * u * v * v;
            A(idx,111) = y * v * v * v;
            A(idx,112) = z * z * z * z;
            A(idx,113) = z * z * z * u;
            A(idx,114) = z * z * z * v;
            A(idx,115) = z * z * u * u;
            A(idx,116) = z * z * u * v;
            A(idx,117) = z * z * v * v;
            A(idx,118) = z * u * u * u;
            A(idx,119) = z * u * u * v;
            A(idx,120) = z * u * v * v;
            A(idx,121) = z * v * v * v;
            A(idx,122) = u * u * u * u;
            A(idx,123) = u * u * u * v;
            A(idx,124) = u * u * v * v;
            A(idx,125) = u * v * v * v;
            A(idx,126) = v * v * v * v;
        end
end

sCoeff = A\sDot;
tCoeff = A\tDot;

fileID = fopen(fullfile(saveDir,'camera0.drg-lfcal'),'w');
for i = 1:length(sCoeff)
   fprintf( fileID, '%e,%e\n', sCoeff(i),tCoeff(i) );
end
fclose(fileID);
    

