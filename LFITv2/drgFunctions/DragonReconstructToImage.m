function [refocusedImage] = DragonReconstructToImage(xVec,yVec,zPos,nImages,filterThreshold,pUV,nCam)
%-------------------------------------------------------------------------------------
% function [refocusedImage] = DragonReconstructToVolume(xVec,yVec,zPos,nImages)
%
% Purpose: Reconstruct a light field to an image 
%          using telecentric refocusing.
% Method:  Calls into Dragon.dll, using DRG_reconstructLightFieldToImage()
%          as defined in Dragon.h
% 
% INPUTS:       xVec:    vector of x-coordinates (object space)
%               yVec:    vector of y-coordinates (object space)
%               zPos:    z-position (object space)
%               nImages: number of images used to build radiance (default = 1)
%                
% OUTPUTS:      refocusedImage: image of refocused intensity indexed by (x,y,z)
%
% NOTABLE SETTINGS:
%   - algorithm:                          0 - INTEGRAL, 1 - FILTERED
%   - intensityThreshold (filtered only): minimum intensity to consider a
%                                         pixel valid
%   - filterThreshold (filtered only):    Validity threshold, all voxels under 
%                                         this value are set to zero.   
%-------------------------------------------------------------------------------------

if nargin == 3
    nImages = 1;
end

if (libisloaded('Dragon'))
    
   nVoxelsX = length(xVec);
   nVoxelsY = length(yVec);
   perUV = pUV;
   camNumber = nCam;
   
   iOut = libpointer('singlePtr',zeros(nVoxelsX*nVoxelsY*nImages,1));
   xp   = libpointer('singlePtr',xVec);
   yp   = libpointer('singlePtr',yVec);
%    
%    if nargin <= 3
%        xcp = [];
%        ycp = [];
%    else
%        xcp = libpointer('singlePtr',xCoeff);
%        ycp = libpointer('singlePtr',yCoeff);
%    end
%    
   if nargin == 7
       fp = libpointer('singlePtr',filterThreshold(:));
   else
       fp = [];
   end
   
   calllib('Dragon','DRG_reconstructLightFieldToImage',zPos,xp,nVoxelsX,yp,nVoxelsY,fp,perUV,camNumber,iOut);
   
   refocusedImage = iOut.val;
   refocusedImage = reshape(refocusedImage,nVoxelsX,nVoxelsY,nImages);
   
   clear xp;
   clear yp;
   clear iOut;
end
