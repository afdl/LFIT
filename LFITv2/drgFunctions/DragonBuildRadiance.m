function [] = DragonBuildRadiance(calibrationFolderName,rawImages,intensityThreshold)
%-------------------------------------------------------------------------------------
% function [] = DragonBuildRadiance(calibrationFolderName,rawImages)
%
% Purpose: Builds the radiance arrays (x,y,u,v,rad) used to generate
%          refocused and perspective data. Note: this algorithm
%          reinterpolates the u,v data onto a plaid grid and supersamples
%          u,v by a factor of 2. 
% Method:  Calls into Dragon.dll, using DRG_buildRadiance() as defined in Dragon.h
% 
% INPUTS:       calibrationFolderName: folder that containes cLocX.txt and cLocY.txt
%               rawImages:             Image data used to build radiance
%                
% OUTPUTS:      none
%-------------------------------------------------------------------------------------

if (libisloaded('Dragon'))
    
   if calibrationFolderName(end) == filesep
       type = 'legacy';
   else
       type = 'new';
   end
   [cLocX, cLocY, nMicroX, nMicroY] = DragonLoadCalibration(calibrationFolderName,type);
    
   rawImageVector = permute(rawImages,[2,1,3]);
   rp  = libpointer('singlePtr',rawImageVector(:));
   if nargin == 3
        tp  = libpointer('singlePtr',intensityThreshold(:));
   else
       tp = [];
   end
   xp  = libpointer('singlePtr',cLocX);
   yp  = libpointer('singlePtr',cLocY);
   
   calllib('Dragon','DRG_buildRadiance',nMicroX,nMicroY,xp,yp,rp,size(rawImages,3),tp);
   
   clear xp;
   clear yp;
   clear rp;
   clear tp;
end
