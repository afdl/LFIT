function [isDragonLoaded] = DragonLoadLibrary(filePath)

if (libisloaded('Dragon'))
    unloadlibrary('Dragon');
end

if exist(fullfile(filePath,'Dragon-matlab.h'), 'file') == 2
    addpath(filePath);
    [notfound,~] = loadlibrary('Dragon','Dragon-matlab.h','addheader','Dragon.h','includepath',filePath);
end

isDragonLoaded = libisloaded('Dragon');
