function [cLocX, cLocY, nMicroX, nMicroY] = DragonLoadCalibration(folderName,type)
%-------------------------------------------------------------------------------------
% function [cLocX, cLocY, nMicroX, nMicroY] = DragonLoadCalibration(folderName,type)
%
% Purpose: Loads calibration data to load into Dragon
% 
% INPUTS:       folderName: either a folder name for 'legacy' or file name for 'new'
%               type:       type of calibration file either 'legacy' or 'new'
%                
% OUTPUTS:      cLocX:   x locations of calibration data
%               cLocY:   y locations of calibration data
%               nMicroX: number of microlenses in calibration data x-dir
%               nMicroY: number of microlenses in calibration data y-dir
%-------------------------------------------------------------------------------------
if nargin == 1
    type = 'new';
end

switch lower(type)
    case 'legacy'

        fileNameX = [folderName,'cLocX.txt'];
        fileNameY = [folderName,'cLocY.txt'];
        
        fileIDX = fopen(fileNameX,'r');
        nMicroX = str2num(fgetl(fileIDX));
        nMicroY = str2num(fgetl(fileIDX));
        cLocX = fscanf(fileIDX,'%f',[1 inf]);
        fclose(fileIDX);
        
        fileIDY = fopen(fileNameY,'r');
        nMicroX = str2num(fgetl(fileIDY));
        nMicroY = str2num(fgetl(fileIDY));
        cLocY = fscanf(fileIDY,'%f',[1 inf]);
        fclose(fileIDY);

    case 'new'

        fileID = fopen(folderName,'r');
        nMicroX = str2num(fgetl(fileID));
        nMicroY = str2num(fgetl(fileID));
        calData = fscanf(fileID,'%f,%f',[2 inf]);
        cLocX = calData(1,:);
        cLocY = calData(2,:);
        fclose(fileID);

    otherwise 
        error('ERROR: DragonLoadCalibrationFromFile, Unsupported calibration file type, use ''legacy'' or ''new''');

end