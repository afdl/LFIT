function [] = DragonSaveApertureSamples(outputFileName)
%-------------------------------------------------------------------------------------
% [] = DragonSaveApertureSamples(outputFileName)
%
% Purpose: Save aperture sample locations for per-perspective calibration
% Method:  Calls into Dragon.dll, using DRG_saveApertureSamples()
%          as defined in Dragon.h
% 
% INPUTS:       outputFileName:  file for exporting data
%                
% OUTPUTS:      none (a file is saved from within dragon)
%-------------------------------------------------------------------------------------
if (libisloaded('Dragon'))

	if nargin<1, outputFileName = []; end
   
	calllib('Dragon','DRG_saveApertureSamples',outputFileName);
	pause(2); % stops matlab from crashing???
   
end