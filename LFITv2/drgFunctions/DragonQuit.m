function [] = DragonQuit()
%-------------------------------------------------------------------------------------
% function [] = DragonQuit()
%
% Purpose: Quits out of the Dragon API
% Method:  Calls into Dragon.dll, using DRG_quit()
%          as defined in Dragon.h
% 
% INPUTS:       none
%                
% OUTPUTS:      none
%
% NOTES:
%   - This needs to be run when you are done using Dragon.
%   - All data that is stored in Dragon will be erased.
%   - Also calls the MATLAB function unloadlibrary() 
%     (Functions from Dragon can no longer be called)
%-------------------------------------------------------------------------------------
pause(2);
if (libisloaded('Dragon'))
   calllib('Dragon','DRG_quit'); 
   unloadlibrary('Dragon') 
end