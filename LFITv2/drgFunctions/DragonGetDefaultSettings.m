function settings = DragonGetDefaultSettings()
% Get reasonable defaults for all Dragon settings.


% Format of these settings
settings.format = 2.0;

% FILEPATH LOCATIONS
settings.filepath.mcal = '';		% Input directory for microlens calibration files: camera0.drg-mcal, camera1.drg-mcal, ...
settings.filepath.lfcal = '';		% Input directory for lightfield calibration files: camera0.drg-lfcal, camera1.drg-lfcal, ...
settings.filepath.uv = '';			% Input directory for user-defined uv samples: camera0.drg-uv, camera1.drg-uv, ...
settings.filepath.images = '';		% Input directory for image folders: camera0/, camera1/, ...
settings.filepath.volumes = '';		% Output directory for volumes
settings.filepath.vectors = '';		% Output directory for vector fields

settings.filepath.imageThresholds = '';			% Input directory for image noise thresholds: camera0.tif, camera1.tif, ... (Filtered only)
settings.filepath.filterThresholds = '';		% Input directory for volume filter thresholds: filter0.bin, filter1.bin (Filtered only)

settings.filepath.imageMask = '';		% Input file for user-defined binary image mask
settings.filepath.volumeMask = '';		% Input file for user-defined binary volume mask

% PROCESSING PARAMETERS
settings.processing.logFile    = 'dragon.log';		% Output file for logging
settings.processing.logLevel   = 3;					% Verbosity level of log, 0 (none) to 5 (very verbose)
settings.processing.maxThreads = -1;				% Maximum number of execution threads, -1 to use all available
settings.processing.useCuda    = false;				% Parallelize using CUDA? (not supported)
settings.processing.calorder   = 1;                 % Calibration polynomial order (0 = 2nd universal, 1 = 3rd universal, 2 = 4th universal,...
                                                    % 3 = 2nd per-perspective, 4 = 3rd per-perspective, 5 = 4th per-perspective

% CAMERAS
settings.camera(1) = DragonConfigureCamera( 'b6640','hexa77', ...
	'magnification',-0.5, 'focalLength',60, 'fNumber',2.67 );

% LIGHT FIELD
settings.lightField.stSamplingFactor = 2.0;				% Supersampling factor on sensor (s,t) plane
settings.lightField.uvSamplingFactor = 1.0;				% Supersampling factor on aperture (u,v) plane
settings.lightField.uvSamplingScheme = 'lines';			% Aperture sampling scheme: standard, lines, spiral, user
settings.lightField.enforceNonNegative   = true;        % Enforce non-negative radiance values?
settings.lightField.useSlidingMeanFilter = false;		% Apply a sliding mean filter to each perspective?

% SIMULATION
settings.simulation.overwrite = false;		% Overwrite existing files?
settings.simulation.camAngles = [];			% Camera angles in [x,y] pairs for simulating lfcal [radians]
settings.simulation.nRays     = 10000;		% Number of rays to trace from point to sensor

% TOMOGRAPHIC RECONSTRUCTION
settings.reconstruction.algorithm          = 'mart';	% Reconstruction algorithm: integral, filtered, mart, art, aart, sart, asart
settings.reconstruction.nIterations        = 10;		% Number of reconstruction iterations (iterative only)
settings.reconstruction.relaxation         = 0.5;		% Relaxation of iterative solution (iterative only)
settings.reconstruction.voxelCutoff        = 1e-5;		% Voxels below this magnitude are skipped, -1 to disable (iterative only) [counts]
settings.reconstruction.outputEveryIter    = false;		% Output every iteration? (iterative only)
settings.reconstruction.enforceNonNegative = false;		% Enforce non-negative voxel values between iterations?

settings.reconstruction.intensityThreshold = 10;		% Global image noise threshold, overridden by filepath.imageThresholds [counts]
settings.reconstruction.filterThreshold    = 0.9;		% Global volume filter threshold, overridden by filepath.filterThresholds
														% Applies to filtered reconstruction and dynamic mask generation

settings.reconstruction.maskingScheme = 'none';			% Volume masking scheme: none, static, dynamic, user
settings.reconstruction.outputMasks   = false;			% Output automatically generated masks?

% REGULARIZATION (ITERATIVE METHODS ONLY)
settings.regularization.algorithm   = 'none';			% Regularization algorithm: none, tv-l1
settings.regularization.nIterations = 30;				% Number of regularization iterations
settings.regularization.penalties   = [10,10,10];		% Penalty terms along x,y,z [counts]

% VOLUME DEFINITION
settings.volume.x = [-38,38,0.15];		% Grid coordinates on the x-axis [mm]
settings.volume.y = [-32,32,0.15];		% Grid coordinates on the y-axis [mm]
settings.volume.z = [-26,26,0.15];		% Grid coordinates on the z-axis [mm]
										% If three elements are provided, arrays will be expanded from [x1,x2,dx] to x1:dx:x2
										% Otherwise, the arrays are used as is

% CROSS-CORRELATION (PIV)
settings.crossCorrelation.useMask = false;		% Mask image or volume?
settings.crossCorrelation.useCrop = false;		% Crop image or volume?

settings.crossCorrelation.weightingFunction     = 'gaussian';		% Window weighting function: uniform, gaussian
settings.crossCorrelation.deformationScheme     = 'sinc7';			% Window deformation scheme: none, bilinear, sinc7, sinc11
settings.crossCorrelation.nValidationIterations = 2;				% Number of vector-replacement validation iterations
settings.crossCorrelation.validationThreshold   = 2;				% Validation threshold (in standard deviations)

settings.crossCorrelation.windowSizes   = [64,32,16,16];		% Window sizes [vox]
settings.crossCorrelation.windowOffsets = [32,16, 8, 8];		% Window offsets [vox]

settings.crossCorrelation.cropX = [0,999];		% Crop limits on the x-axis [index]
settings.crossCorrelation.cropY = [0,999];		% Crop limits on the y-axis [index]
settings.crossCorrelation.cropZ = [0,999];		% Crop limits on the z-axis [index]

settings.crossCorrelation.limitsU = [-99,99];	% Velocity limits in the x-direction [vox/dt]
settings.crossCorrelation.limitsV = [-99,99];	% Velocity limits in the y-direction [vox/dt]
settings.crossCorrelation.limitsW = [-99,99];	% Velocity limits in the z-direction [vox/dt]

% OPTICAL FLOW
settings.opticalFlow.algorithm = 'farneback';			% Optical flow algorithm: farneback
settings.opticalFlow.weightingFunction = 'gaussian';	% Block weighting function: uniform, gaussian

settings.opticalFlow.pyramidScale = 0.5;	% Scale (<1) to build pyramids for each image
settings.opticalFlow.pyramidDepth = 2;		% Number of pyramid layers including the initial image
settings.opticalFlow.windowSize   = 3;		% Averaging window size
settings.opticalFlow.nIterations  = 3;		% Number of iterations PER pyramid level
settings.opticalFlow.polyN = 5;				% Size of pixel neighborhood used to find polynomial expansion
settings.opticalFlow.polyS = 1.2;			% Standard deviation of Gaussian smoothing

settings.opticalFlow.useCrop = false;		% Crop image?
settings.opticalFlow.cropX = [0,999];		% Crop limits on the x-axis [index]
settings.opticalFlow.cropY = [0,999];		% Crop limits on the y-axis [index]
settings.opticalFlow.cropZ = [0,999];		% Crop limits on the z-axis [index]
