function [x,y] = turtle(nSamples,mainDir,maxDiameter)

dx = 1;
dy = 1;

x = zeros(nSamples,1);
y = zeros(nSamples,1);

i = 2;
n = 1;

while (i <= nSamples)
	for j = 1:n
		x(i) = x(i-1);
		y(i) = y(i-1) + dy;
		i = i + 1;
		if (i > nSamples)
			break;
		end
	end
	if (i > nSamples)
		break
	end
	for j = 1:n
		x(i) = x(i-1) + dx;
		y(i) = y(i-1);
		i = i + 1;
		if (i > nSamples)
			break;
		end
	end
	n = n + 1;
	dx = -dx;
	dy = -dy;
end

if nargin==3
	crop = ( sqrt(x.^2+y.^2) > maxDiameter/2 );
	x(crop) = [];
	y(crop) = [];
	nSamples = length(x);
end

if ~isempty(mainDir)
	fileID = fopen(fullfile(mainDir,'uvSamples.txt'),'wt');
	for i = 1:nSamples
		fprintf(fileID,'%d %d\n',x(i),y(i));
	end
	fclose(fileID);
end
