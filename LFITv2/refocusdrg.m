% Refocus each calibration target to its respective plane. The resulting
% set of images should have no wobble in the dots/corner points.
function [  ] = refocusdrg(F_m,M,FN,zVal,...
    xMin, xMax, yMin, yMax, polyType, calOrder, uvSamplingScheme, imageDir, imageName, lfcalDir, mcalDir, DragonFilePath, ...
    px_x, px_y, ml_x, ml_y, p_p, p_ml, f_ml, outputDir, cmap, saveIM)

%% File Paths
mcalFile = [mcalDir, '\camera0.drg-mcal'];
lfcalDir = strrep(lfcalDir,'\','/');
settingsFile = [imageDir '\settings.txt'];
camNumber = 0; %i.e. camera0.drg, camera1.drg, camera2.drg...

%% Redefine parameters for dragon
if strcmp(polyType, 'universal') == 1
    calorder = calOrder - 2; %0 = 2nd, 1 = 3rd (default), 2 = 4th, 3 = 2nd-uv, 4 = 3rd-uv, 5 = 4th-uv
elseif strcmp(polyType, 'uv') == 1
    calorder = calOrder + 1;
end

%%
resX = ml_x;
resY = ml_x*px_y/px_x;

% Load Dragon Library
isDragonLoaded = DragonLoadLibrary(DragonFilePath);

% Settings Init
settings = DragonGetDefaultSettings(); % helper function to init all settings values

% Construct x,y vectors from settings
xVec = linspace( xMin, xMax, resX );
yVec = linspace( yMin, yMax, resY );

settings.volume.x = [xMin, xMax]; 
settings.volume.y = [yMin, yMax];

settings.reconstruction.algorithm  = 'integral'; 
settings.lightField.uvSamplingScheme = 'lines'; %default of lines is good for universal cal
settings.camera.focalLength          = F_m;
settings.camera.magnification        = M;
settings.camera.fNumber              = FN;
settings.camera.pixelCount(1)        = px_x;
settings.camera.pixelCount(2)        = px_y;
settings.camera.microlensCount(1)    = ml_x;
settings.camera.microlensCount(2)    = ml_y;
settings.camera.pixelPitch           = p_p;
settings.camera.microlensPitch       = p_ml;
settings.camera.microlensFocalLength = f_ml;

settings.processing.logLevel = 3;
settings.processing.calorder = calorder; 
settings.filepath.lfcal = lfcalDir;

if calorder == 3 || calorder == 4 || calorder == 5
    settings.lightField.uvSamplingScheme = uvSamplingScheme; 
    perUV = 1;
else
    perUV = 0;
end

DragonWriteSettingsFile(settingsFile, settings)
DragonInitFromSettingsFile(settingsFile);

%%
[~, imname, ~] = fileparts(imageName);
imageFile = fullfile(imageDir,imageName); 
outputDir = fullfile(outputDir,'RefocusDLFC');
if ~exist(outputDir)
    mkdir(outputDir)
end

rawImages = 65535*imadjust(im2double(imread(imageFile)));
fprintf('Calculating Light Field...')
DragonBuildRadiance(mcalFile,rawImages);
fprintf('\t\tComplete.\n')
image = DragonReconstructToImage(xVec,yVec,zVal,1,[],perUV,camNumber);

% Display image
figure
displayimage(image','',cmap,[1 1 1]);

% Save image
if saveIM == 1
    imwrite(image'/max(image(:)),[outputDir '\' imname 'z' num2str(zVal) '.png']);
end
pause(1)

DragonQuit;