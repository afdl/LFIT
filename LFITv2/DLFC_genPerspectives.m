% Generate Perspectives for Light Field Calibration
function [  ] = DLFC_genPerspectives(f_m,M,fnum_m,zVals,turtsampling, uvSamplingScheme, ...
    nPixelX, nPixelY, mainDir, mcalDir, DragonFilePath, ... 
    px_x, px_y, ml_x, ml_y, p_p, p_ml, f_ml)



%% SETUP
mcalFile = [mcalDir, '\camera0.drg-mcal'];

% UV sampling
if strcmp(uvSamplingScheme,'spiral') == 1 || strcmp(uvSamplingScheme,'lines') == 1 
    dragonUVsampling = 1; % 1 = use dragon sampling (required for per-uv cal)
    uvOutputName   = [mainDir, '/uv.txt'];
else 
    dragonUVsampling = 0; % 0 = use turtle sample
end
    

%% INITIALIZE DRAGON

% Initialize settings file
settings     = DragonGetDefaultSettings();  % helper function to init all settings values
settingsFile = fullfile(mainDir,'settings.txt');
DragonWriteSettingsFile(settingsFile,settings);

% Load Dragon and initialize global settings
if DragonLoadLibrary(DragonFilePath)
    DragonInitFromSettingsFile(settingsFile);
else
    error('Dragon Library not loaded. Ensure that the filepath provided to DragonLoadLibrary was correct.');
end

% Update settings (from ExampleDataInfo.txt)
settings.camera.focalLength          = f_m;
settings.camera.magnification        = M;
settings.camera.fNumber              = fnum_m;
settings.camera.pixelCount(1)        = px_x;
settings.camera.pixelCount(2)        = px_y;
settings.camera.microlensCount(1)    = ml_x;
settings.camera.microlensCount(2)    = ml_y;
settings.camera.pixelPitch           = p_p;
settings.camera.microlensPitch       = p_ml;
settings.camera.microlensFocalLength = f_ml;
    
if dragonUVsampling == 1
    settings.lightField.uvSamplingScheme = uvSamplingScheme;
end

DragonWriteSettingsFile(settingsFile,settings)
DragonInitFromSettingsFile(settingsFile);


%% GENERATE PERSPECTIVES AT EACH Z-DEPTH

for ind = 1:numel(zVals)
    zVal = zVals(ind);
    
    zDir      = fullfile(mainDir,sprintf('%gmm',zVal));
    imageFile = fullfile(zDir,'average.tif');
    saveDir   = dirchk(fullfile(zDir,'Perspectives',filesep));

    % Load raw image
    rawImage = double(imread(imageFile));
    
    % Build radiance arrays
    DragonBuildRadiance(mcalFile,rawImage);
    
    % Generate x,y data based on CCD
	xVec = linspace( -settings.camera.pixelCount(1)*settings.camera.pixelPitch/2, settings.camera.pixelCount(1)*settings.camera.pixelPitch/2, nPixelX );
    yVec = linspace( -settings.camera.pixelCount(2)*settings.camera.pixelPitch/2, settings.camera.pixelCount(2)*settings.camera.pixelPitch/2, nPixelY );
    
    if dragonUVsampling == 1
        % Get perspective sweep from Dragon
        DragonSaveApertureSamples(uvOutputName);
        uvVec = load(uvOutputName);
        uVec = uvVec(:,1);
        vVec = uvVec(:,2);
    else
        % Get perspective sweep from Turtle
        si = (1 - settings.camera.magnification)*settings.camera.focalLength;
        sizeOfPixelOnAperture = si * settings.camera.pixelPitch / settings.camera.microlensFocalLength;
        [uVec,vVec] = turtle(turtsampling^2,mainDir);
        uVec = uVec*sizeOfPixelOnAperture;
        vVec = vVec*sizeOfPixelOnAperture;
    end
    
    perspectiveSweep = DragonGeneratePerspectiveViews( xVec,yVec, uVec,vVec );
    
    % Output perspective sweep
    for i = 1:length(uVec)
        imwrite( uint16(perspectiveSweep(:,:,i)'), fullfile(saveDir,sprintf('%04d.tif',i-1)) );
    end
end

fileID = fopen(fullfile(mainDir,'uvpositions.txt'),'w');
for i = 1:length(uVec)
   fprintf( fileID, '%f %f\n', uVec(i),vVec(i) );
end
fclose(fileID);