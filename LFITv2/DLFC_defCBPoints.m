% Find checkerboard calibration points for DLFC
% Last updated: Mar2021
function imUsed2 = DLFC_defCBPoints(mainDir, depthLocation, run2, topleftCoord, diagnoseAllPerspectives,...
    squareSize, nPixelsX, nPixelsY, imUsed2, px_x, px_y, p_p)
%-------------------------------------------------------------------
%% HS Inputs
%-------------------------------------------------------------------
tic
sensorPixelX = px_x;
sensorPixelY = px_y;
pixelPitch   = p_p;

%-------------------------------------------------------------------
%% 
%-------------------------------------------------------------------
depthDir       = fullfile( mainDir, sprintf('%gmm',depthLocation) );
perspectiveDir = fullfile( depthDir, 'Perspectives' );
resultsDir     = fullfile( mainDir, 'Results', filesep );
outputFile     = fullfile( resultsDir, sprintf('%+g.drg-dots',depthLocation) );
a = dir([perspectiveDir '/*.tif']);
numPerspectives = size(a,1);

if run2 == 1
    perspectiveDir = fullfile( depthDir, 'PerspectivesRun2' );
end


%-------------------------------------------------------------------
%% Read in Perspective images & find intersection points
%-------------------------------------------------------------------
CB = imageDatastore(perspectiveDir); 
im = readimage(CB,1); 
imSize = [size(im,1),size(im,2)];
[imagePoints,boardSize,imUsed] = detectCheckerboardPoints(CB.Files);
% Generate object-space points matrix
worldPoints = generateCheckerboardPoints(boardSize,squareSize);
worldPoints(:,1) = worldPoints(:,1);
worldPoints(:,2) = worldPoints(:,2);
worldPoints(:,3) = depthLocation;
worldPoints = repmat(worldPoints, 1, 1, size(imagePoints,3));

%%% Account for 0,0 point being somewhere other than top-left
for uv = 1:size(imagePoints, 3)
    if imagePoints(1,1,uv) > imSize(2)/2 && imagePoints(1,2,uv) > imSize(1)/2 %if 0,0 is in bottom right
        imagePoints(:,:,uv) = flipud(imagePoints(:,:,uv));
    
    elseif imagePoints(1,1,uv) < imSize(2)/2 && imagePoints(1,2,uv) > imSize(1)/2 %if 0,0 is in bottom left
        worldPointsTemp(:,1) = worldPoints(:,2,uv);
        worldPointsTemp(:,2) = worldPoints(:,1,uv);
        worldPoints(:,:,uv) = [worldPointsTemp, worldPoints(:,3,uv)];
        p = 1;
        neworder = [];
        for col = 1:boardSize(2)-1
            for row = 1:boardSize(1)-1
                neworder(p) = size(imagePoints,1) - (boardSize(1)-1)*col + row;
                p = p + 1;
            end
        end
        for q = 1:p-1
            imagePointsTemp(q,:) = imagePoints(neworder(q),:,uv);
        end
        imagePoints(:,:,uv) = imagePointsTemp;
        
    elseif imagePoints(1,1,uv) > imSize(2)/2 && imagePoints(1,2,uv) < imSize(1)/2 %if 0,0 is in top right
        p = 1;
        neworder = [];
        worldPointsTemp(:,1) = worldPoints(:,2,uv);
        worldPointsTemp(:,2) = worldPoints(:,1,uv);
        worldPoints(:,:,uv) = [worldPointsTemp, worldPoints(:,3,uv)];
        for col = 1:boardSize(2)-1
            for row = 1:boardSize(1)-1
                neworder(p) = boardSize(1)*col - row-col+1;
                p = p + 1;
            end
        end
        for q = 1:p-1
            imagePointsTemp(q,:) = imagePoints(neworder(q),:,uv);
        end
        imagePoints(:,:,uv) = imagePointsTemp;
    end
end
worldPoints(:,1,:) = worldPoints(:,1,:) + topleftCoord(1);
worldPoints(:,2,:) = worldPoints(:,2,:) + topleftCoord(2);

%%% Account for top-left point popping in and out of FoV across accepted perspectives (e.g. due to vignetting)
minx = min(min(imagePoints(:,1,:)));
miny = min(min(imagePoints(:,2,:)));
maxx = max(max(imagePoints(:,1,:)));
maxy = max(max(imagePoints(:,2,:)));
for uv = 1:size(imagePoints, 3)   
    deltax = (maxx - minx)/(boardSize(2)-1); 
    deltay = (maxy - miny)/(boardSize(1)-1); 
    if imagePoints(1,1,uv) > (minx + deltax/2) 
        worldPoints(:,:,uv) = [worldPoints(:,1,uv) + squareSize, worldPoints(:,2,uv), worldPoints(:,3,uv)];
    end
    if imagePoints(1,2,uv) > (miny + deltay/2) 
        worldPoints(:,:,uv) = [worldPoints(:,1,uv), worldPoints(:,2,uv) + squareSize, worldPoints(:,3,uv)];
    end
    if imagePoints(1,1,uv) > (minx + deltax*3/2) 
        worldPoints(:,:,uv) = [worldPoints(:,1,uv) + squareSize, worldPoints(:,2,uv), worldPoints(:,3,uv)];
    end
    if imagePoints(1,2,uv) > (miny + deltay*3/2) 
        worldPoints(:,:,uv) = [worldPoints(:,1,uv), worldPoints(:,2,uv) + squareSize, worldPoints(:,3,uv)];
    end
end


%%% Copy unused boards into separate folder in case we want to process them too
if run2 == 0 && size(imagePoints, 3) < numPerspectives
    mkdir([depthDir, '\PerspectivesRun2'])
    for k = 1:size(imUsed, 1)
        if imUsed(k) == 0
            copyfile(char(CB.Files(k)), [depthDir, '\PerspectivesRun2\' sprintf('%04g',k) '.tif'])
        else
        end
    end
end

%-------------------------------------------------------------------
%% Load u & v vectors from genperspectives & filter out any unused perspectives
%-------------------------------------------------------------------
fileID = fopen(fullfile(mainDir,'uvpositions.txt'),'r');
vars3 = fscanf(fileID,'%f %f',[2 inf]);
fclose(fileID)

uPos = [];
vPos = [];
if run2 == 0
    for j = 1:size(vars3,2)
        if imUsed(j) == 1
            uPos = [uPos; vars3(1,j)];
            vPos = [vPos; vars3(2,j)];
        else
        end
    end
else %i.e. if run2 = 1
    n = 1;
	for j = 1:size(vars3,2)
        if imUsed2(j) == 0 % if perspective j was rejected in the first run
            if imUsed(n) == 1 % if it was accepted this time
                uPos = [uPos; vars3(1,j)];
                vPos = [vPos; vars3(2,j)];
                n = n + 1;
            else
                n = n + 1;
            end
        end
	end
end

%-------------------------------------------------------------------
%% Interpolate image points onto sensor, store corresponding world points and uv coordinates
%-------------------------------------------------------------------
idx = 1;
for uv = 1:size(imagePoints,3)
    
    uLoc(idx:idx + size(worldPoints, 1) - 1, :) = uPos(uv);
    vLoc(idx:idx + size(worldPoints, 1) - 1, :) = vPos(uv);
    xLocAct(idx:idx + size(worldPoints, 1) - 1, :) = worldPoints(:,1,uv);
    yLocAct(idx:idx + size(worldPoints, 1) - 1, :) = worldPoints(:,2,uv);
    zLocAct(idx:idx + size(worldPoints, 1) - 1, :) = worldPoints(:,3,uv);
    
    %Interpolate
    xLocImg = imagePoints(:,1,uv);
    yLocImg = imagePoints(:,2,uv);
    
    sSSRange = zeros(nPixelsX,1);
    tSSRange = zeros(nPixelsY,1);

    xMin = -(sensorPixelX * pixelPitch) / 2.0;
    xMax =  (sensorPixelX * pixelPitch) / 2.0;
    dx = (xMax - xMin) / (nPixelsX - 1.0);
    for i = 1:nPixelsX
        sSSRange(i) = xMin + dx * (i-1);
    end

    yMin = -(sensorPixelY * pixelPitch) / 2.0;
    yMax =  (sensorPixelY * pixelPitch) / 2.0;
    dy = (yMax - yMin) / (nPixelsY - 1.0);
    for i = 1:nPixelsY
        tSSRange(i) = yMin + dy * (i-1);
    end
    xLocImgmm = [];
    yLocImgmm = [];
    xLocImgmm = [xLocImgmm; interp1(1:nPixelsX,sSSRange,xLocImg)];
    yLocImgmm = [yLocImgmm; interp1(1:nPixelsY,tSSRange,yLocImg)];

    sLocImg(idx:idx + size(worldPoints, 1) - 1, :) = xLocImgmm;
    tLocImg(idx:idx + size(worldPoints, 1) - 1, :) = yLocImgmm;
                
    idx = idx + size(worldPoints, 1);
end

%-------------------------------------------------------------------
%% %Show detected points on first perspective image for debugging
%-------------------------------------------------------------------

uvidx = 1;
for uv = 1:length(CB.Files)
    if imUsed(uv) == 1
        figure
        im = readimage(CB,uv); 
        imshow(im, [min(min(im)) max(max(im))]);
        hold on;
        plot(imagePoints(:,1,uvidx),imagePoints(:,2,uvidx),'go');
        for a = 1:size(imagePoints, 1)
            wp = sprintf('x = %g \n y = %g', worldPoints(a,1,uvidx), worldPoints(a,2,uvidx));
            text(imagePoints(a,1,uvidx),imagePoints(a,2,uvidx), ['\bf' wp], 'color', 'magenta', 'fontsize', 11)
        end
        uvidx = uvidx + 1;
        if diagnoseAllPerspectives == 0
            break
        end
            
    end
end

%-------------------------------------------------------------------
%% Output dot locations to file
%-------------------------------------------------------------------
if run2 == 1 %if we already have points for this plane from first run, append data to existing drg-dots
    fileID = fopen(outputFile,'at');
    for i = 1:length(sLocImg)
        fprintf( fileID, '%f,%f,%f,%f,%f,%f,%f\n', sLocImg(i),tLocImg(i), xLocAct(i),yLocAct(i),zLocAct(i), uLoc(i),vLoc(i) );
    end
    fclose(fileID);
else
    fileID = fopen(outputFile,'wt');
    for i = 1:length(sLocImg)
        fprintf( fileID, '%f,%f,%f,%f,%f,%f,%f\n', sLocImg(i),tLocImg(i), xLocAct(i),yLocAct(i),zLocAct(i), uLoc(i),vLoc(i) );
    end
    fclose(fileID);
end
if run2 == 0
    imUsed2 = [];
    imUsed2 = imUsed; %save copy of variable to use in case run2 is needed
    fprintf('Number of used perspectives is %g \n', size(imagePoints,3));
end
toc

%% Tips: 

% When checkerboard is a square (i.e. same number of rows and columns), matlab can get confused about orientation
% Use plot generated at end to check worldPoints are assigned correctly

% Make sure CB.Files is storing perspectives in the correct order
% (i.e. same order as genPerspectives output (0000.tif, 0001.tif ...)

% topleftCoord = [x,y] coordinates for the top-left point - i.e. our origin
% (pay attention to boardSize - If it's showing the expected number of squares 
% then leave as [0, 0]. If not, may have to adjust topleftCoord if our 
% origin is outside FoV

% If number of perspectives used is less than total number of perspectives then
% unused perspectives get copied to a separate folder. We may want to define these
% separately - when Matlab looks through a folder, it looks for most common sized 
% checkerboard so for e.g. if CB point is close to edge and leaves FoV in
% some perspectives, a different-sized grid is detected. By copying these to a different
% folder, we can rerun the calibration for the different grid size and
% append those points to the existing drg-dots file for that plane.
% If you do wish to run the rejected perspectives separately, just set run2 = 1,
% else run2 = 0 - don't need to change any directory locations. Look through images 
% in PerspectivesRun2 folder to ensure all missing points are on same side

% (note: imUsed2 has to still be a variable in the workspace if run2 = 1). 
% Remember to adjust topleftCoord for run2 if needed, go look at new perspectives folder 
% to see what this should be