function [  ] = DLFC_calcCoeffsUV(mainDir, planes, order)

%% Filepaths
saveDir = [mainDir, '\Results\'];
uvPositions = [mainDir, '\uvpositions.txt'];

%% Read in data
xAct = [];
yAct = [];
zAct = [];
sDot = [];
tDot = [];
uDot = [];
vDot = [];

%Load dot locations
for i = 1:length(planes)
    fileID = fopen([saveDir num2str(planes(i),'%+g') '.drg-dots'],'r');
    vars = fscanf(fileID,'%f,%f,%f,%f,%f,%f,%f',[7 inf]);
    fclose(fileID);
    
    for j = 1:size(vars,2)
        if ~isinf(vars(3,j))
            sDot = [sDot; vars(1,j)];
            tDot = [tDot; vars(2,j)];
            
            xAct = [xAct; vars(3,j)];
            yAct = [yAct; vars(4,j)];
            zAct = [zAct; vars(5,j)];
            
            uDot = [uDot; vars(6,j)];
            vDot = [vDot; vars(7,j)];
        end
    end
    
end

figure; scatter3( xAct,yAct,zAct );

uvVec = load(uvPositions);
uVec = uvVec(:,1);
vVec = uvVec(:,2);
        
% Sort coordinates by perspective so we have a start and end reference
% point for the loop
drgdotunsorted = [sDot tDot xAct yAct zAct uDot vDot];
drgdots = sortrows(drgdotunsorted, [6 7]);
[C,ia,ic] = unique(drgdots(:,6:7), 'rows', 'stable'); % C = unique u and v coordinates, ia = first index of each unique coordinate, ic = relates 'ia' to drgdots
count = [C, accumarray(ic,1)]; % shows how many points belong to each perspective

% Transformation matrix
A = zeros(length(xAct),10);

% Initial start and finish point
start = 1;
finish = count(1,3);

switch order
    case 2
        for persp = 1:length(C)      
            for idx = start:finish  %iterating thru where that perspective starts & where it ends
                x = drgdots(idx, 3);
                y = drgdots(idx, 4);
                z = drgdots(idx, 5);

                A(idx,1)  = 1;
                A(idx,2)  = x;
                A(idx,3)  = y;
                A(idx,4)  = z;
                A(idx,5)  = x * x;
                A(idx,6)  = x * y;
                A(idx,7)  = x * z;
                A(idx,8)  = y * y;
                A(idx,9)  = y * z;
                A(idx,10) = z * z;
            end

            sCoeff_temp = A(start:finish,:)\drgdots(start:finish, 1);
            tCoeff_temp = A(start:finish,:)\drgdots(start:finish, 2);

            %pop the coefficients into different cells for each perspective (but
            %stored in one variable) - access nth perspective with xCoeffs{[n]}
            sCoeffscell{persp} = sCoeff_temp;   
            tCoeffscell{persp} = tCoeff_temp;
            
            %update start and finish points
            if persp < length(C)  %making sure it cuts off at last perspective
                start = start + count(persp,3);
                finish = finish + count(persp+1,3);
            end
        end

    case 3
        for persp = 1:length(C)    
            for idx = start:finish 
                x = drgdots(idx, 3);
                y = drgdots(idx, 4);
                z = drgdots(idx, 5);

                A(idx,1)  = 1;
                A(idx,2)  = x;
                A(idx,3)  = y;
                A(idx,4)  = z;
                A(idx,5)  = x * x;
                A(idx,6)  = x * y;
                A(idx,7)  = x * z;
                A(idx,8)  = y * y;
                A(idx,9)  = y * z;
                A(idx,10) = z * z;
                A(idx,11) = x * x * x;
                A(idx,12) = x * x * y;
                A(idx,13) = x * x * z;
                A(idx,14) = x * y * y;
                A(idx,15) = x * y * z;
                A(idx,16) = x * z * z;
                A(idx,17) = y * y * y;
                A(idx,18) = y * y * z;
                A(idx,19) = y * z * z;
                A(idx,20) = z * z * z;
            end

            sCoeff_temp = A(start:finish,:)\drgdots(start:finish, 1);
            tCoeff_temp = A(start:finish,:)\drgdots(start:finish, 2);

            sCoeffscell{persp} = sCoeff_temp;   
            tCoeffscell{persp} = tCoeff_temp;

            if persp < length(C)  
                start = start + count(persp,3);
                finish = finish + count(persp+1,3);
            end
        end

    case 4
        for persp = 1:length(C)        
            for idx = start:finish 
                x = drgdots(idx, 3);
                y = drgdots(idx, 4);
                z = drgdots(idx, 5);
                
                A(idx,1)  = 1;
                A(idx,2)  = x;
                A(idx,3)  = y;
                A(idx,4)  = z;
                A(idx,5)  = x * x;
                A(idx,6)  = x * y;
                A(idx,7)  = x * z;
                A(idx,8)  = y * y;
                A(idx,9)  = y * z;
                A(idx,10) = z * z;
                A(idx,11) = x * x * x;
                A(idx,12) = x * x * y;
                A(idx,13) = x * x * z;
                A(idx,14) = x * y * y;
                A(idx,15) = x * y * z;
                A(idx,16) = x * z * z;
                A(idx,17) = y * y * y;
                A(idx,18) = y * y * z;
                A(idx,19) = y * z * z;
                A(idx,20) = z * z * z;
                A(idx,21) = x * x * x * x;
                A(idx,22) = x * x * x * y;
                A(idx,23) = x * x * x * z;
                A(idx,24) = x * x * y * y;
                A(idx,25) = x * x * y * z;
                A(idx,26) = x * x * z * z;
                A(idx,27) = x * y * y * y;
                A(idx,28) = x * y * y * z;
                A(idx,29) = x * y * z * z;
                A(idx,30) = x * z * z * z;
                A(idx,31) = y * y * y * y;
                A(idx,32) = y * y * y * z;
                A(idx,33) = y * y * z * z;
                A(idx,34) = y * z * z * z;
                A(idx,35) = z * z * z * z;
            end

            sCoeff_temp = A(start:finish,:)\drgdots(start:finish, 1);
            tCoeff_temp = A(start:finish,:)\drgdots(start:finish, 2);

            sCoeffscell{persp} = sCoeff_temp;   
            tCoeffscell{persp} = tCoeff_temp;

            if persp < length(C) 
                start = start + count(persp,3);
                finish = finish + count(persp+1,3);
            end
        end
end

if ~exist([saveDir '\lfcal\'], 'dir')
    mkdir([saveDir '\lfcal\'])
end
outputDir = [saveDir, '\lfcal\'];

% Output coeffs to separate files
for persp = 1:length(C)
    filename = sprintf('camera0_%g.drg-lfcal', persp-1);
    fileID = fopen(fullfile(outputDir,filename),'w');
    for i = 1:length(sCoeffscell{persp})
        fprintf( fileID, '%e,%e\n', sCoeffscell{persp}(i),tCoeffscell{persp}(i) );
    end
    fclose(fileID);
end

% Rename files to perspective match turtle
for k = 1:length(C)
     [~, idx] = min(sqrt((C(k,1) - uVec).^2 + (C(k,2) - vVec).^2));
     fix(k, :) = idx;
end
for persp = 1:length(C)
    oldfilename = sprintf('camera0_%g.drg-lfcal', persp-1);
    newfilename = sprintf('camera0_persp%g.drg-lfcal', fix(persp)-1);
    oldfilepath  = [outputDir oldfilename];
    newfilepath  = [outputDir newfilename];
    copyfile(oldfilepath, newfilepath)
    delete(oldfilepath);
end